#include "train_velocity.h"

struct fraction fraction_multiply(struct fraction f1, struct fraction f2) {
	struct fraction ret_val;
	ret_val.numerator = f1.numerator * f2.numerator;
	ret_val.denominator = f1.denominator * f2.denominator;
	return ret_val;
}

struct fraction fraction_divide(struct fraction f1, struct fraction f2) {
	struct fraction ret_val;
	ret_val.numerator = f1.numerator * f2.denominator;
	ret_val.denominator = f1.denominator * f2.numerator;
	return ret_val;
}

struct fraction fraction_add(struct fraction f1, struct fraction f2) {
	struct fraction ret_val;
	ret_val.numerator = (f1.numerator * f2.denominator) + (f2.numerator * f1.denominator);
	ret_val.denominator = f1.denominator * f2.denominator;
	return ret_val;
}

struct fraction fraction_subtract(struct fraction f1, struct fraction f2) {
	struct fraction ret_val;
	ret_val.numerator = (f1.numerator * f2.denominator) - (f2.numerator * f1.denominator);
	ret_val.denominator = f1.denominator * f2.denominator;
	return ret_val;
}

int less_than(struct fraction f1, struct fraction f2) {
	return f1.numerator * f2.denominator < f2.numerator * f1.denominator;
}

// v_0 is original speed, v_1 is new speed
int acceleration_calculation(int distance, int time, int v_0, int v_1) {
	// two cases, either
	// 1. we didn't fully reach v_1 crossing the point, or
	// 2. we hit v_1 before we crossed the point

	// int v_overall = (distance) / time;
	// int v_a = (v_0 + v_1) / 2;
	struct fraction v_overall = { .numerator = distance, .denominator = time };
	struct fraction v_a = { .numerator = v_0 + v_1, .denominator = 2 };

	struct fraction distance_frac = { .numerator = distance, .denominator = 1 };
	struct fraction time_frac = { .numerator = time, .denominator = 1 };
	struct fraction v_0_frac = { .numerator = v_0, .denominator = 100 }; // mm/tick
	struct fraction v_1_frac = { .numerator = v_1, .denominator = 100 }; // mm/tick

	// if ((v_a > 0 && (v_overall < v_a)) || (v_a < 0 && (v_overall > v_a))) {
	// // case 1
	// // d = v_0*t + 1/2*a*t^2
	// // a = 2*(d - v_0*t)/t^2
	// return 2 * (distance - (v_0 * time)) / (time * time);
	// } else {
	// // case 2
	// // d = d1 + d2
	// // t = t1 + t2
	// // va = d1 / t1
	// // v2 = d2 / t2
	// return (distance - (v_1 * time)) / (v_a - v_1);
	// }
	struct fraction constant_0 = { .numerator = 0, .denominator = 1 };
	if ((less_than(constant_0, v_a) && less_than(v_overall, v_a))
		|| (less_than(v_a, constant_0) && less_than(v_a, v_overall))) {
		struct fraction constant_2 = { .numerator = 2, .denominator = 1 };
		struct fraction res = fraction_divide(
			fraction_multiply(constant_2, fraction_subtract(distance_frac, fraction_multiply(v_0_frac, time_frac))),
			fraction_multiply(time_frac, time_frac));
		return (res.numerator * (100 * 100)) / res.denominator; // mm/tick^2 -> mm/s^2
	} else {
		struct fraction res = fraction_divide(fraction_subtract(distance_frac, fraction_multiply(v_1_frac, time_frac)),
											  fraction_subtract(v_a, v_1_frac));
		return (res.numerator * (100 * 100)) / res.denominator; // mm/tick^2 -> mm/s^2
	}
	ASSERT(0);
	return 0;
}

int stopping_time_from_acceleration(int velocity, int acceleration) {
	acceleration *= -1;
	ASSERT(acceleration * velocity > 0);
	struct fraction velocity_frac = { .numerator = velocity, .denominator = 1 };
	struct fraction acceleration_frac = { .numerator = acceleration, .denominator = 1 };

	struct fraction res = fraction_divide(velocity_frac, acceleration_frac);
	return (res.numerator * 100) / res.denominator;
}

int stopping_distance_from_acceleration(int velocity, int acceleration) {
	struct fraction velocity_frac = { .numerator = velocity, .denominator = 1 };
	struct fraction acceleration_frac = { .numerator = acceleration, .denominator = 1 };

	struct fraction constant_neg_1 = { .numerator = -1, .denominator = 1 };
	struct fraction constant_half = { .numerator = 1, .denominator = 2 };
	struct fraction time_frac = fraction_multiply(fraction_divide(velocity_frac, acceleration_frac), constant_neg_1);

	struct fraction res = fraction_add(fraction_multiply(velocity_frac, time_frac),
									   fraction_multiply(fraction_multiply(constant_half, acceleration_frac),
														 fraction_multiply(time_frac, time_frac)));

	return res.numerator / res.denominator;
}