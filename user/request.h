#ifndef REQUEST_H
#define REQUEST_H

#include "util/debug.h"
#include "util/rpi.h"
#include "util/util.h"
#include <stdint.h>

/******** Request for Syscalls *********/
enum syscall {
	CREATE,
	MY_TID,
	PARENT_TID,
	YIELD,
	EXIT,
	SEND,
	RECEIVE,
	REPLY,
	AWAIT_EVENT,
	IDLE,
	WRITE_REGISTER,
	READ_REGISTER,
	GET_IDLE_TIME,
	QUIT,
};

struct Syscall_Request {
	uint64_t request_id;
	uint64_t arg1;
	uint64_t arg2;
	uint64_t arg3;
	uint64_t arg4;
	uint64_t arg5;
};

struct Syscall_Request
create_request(enum syscall request_id, uint64_t arg1, uint64_t arg2, uint64_t arg3, uint64_t arg4, uint64_t arg5);

/******** Request for Name Server *********/
enum name_server_request {
	NAME_SERVER_REGISTER,
	NAME_SERVER_UNREGISTER,
	NAME_SERVER_WHOIS,
};

struct Name_Server_Request {
	char request_type;
	char name[100];
	int tid;
};

struct Name_Server_Request create_name_server_request(const char name_server_request, const char* name, const int tid);

/******** Request for RPS Server *********/
enum rps {
	RPS_SIGNUP,
	RPS_PLAY,
	RPS_QUIT,
	RPS_ROCK,
	RPS_PAPER,
	RPS_SCISSORS,
};

struct RPS_Request {
	uint8_t request_id;
	uint8_t weapon;
};

struct RPS_Request create_rps_request(uint8_t request_id, uint8_t weapon);

/******** Request for Clock Server *********/

enum clock_server_request {
	CLOCK_SERVER_TIME,
	CLOCK_SERVER_DELAY,
	CLOCK_SERVER_DELAY_UNTIL,
	CLOCK_SERVER_UPDATE_TIME,
};

struct Clock_Server_Request {
	enum clock_server_request request_type;
	uint32_t arg1;
};

struct Clock_Server_Request create_clock_request(enum clock_server_request request_type, uint32_t arg1);

/******** Request for UART Server *********/
// TODO: dont need the _0/_1/_term/_train, just send to appropriate server
enum uart_server_request {
	UART_TX_TRIGGERED_0,
	UART_RX_TRIGGERED_0,
	UART_TX_TRIGGERED_1,
	UART_RX_TRIGGERED_1,
	UART_CTS_TRIGGERED,
	UART_SEND_TERM,
	UART_SEND_TRAIN,
	UART_GET_TERM,
	UART_GET_TRAIN,
	UART_TX_GO_BACK_TO_READY,
	UART_RECEIVING_SENSOR_BYTES,
	UART_DONE_RECEIVING_SENSOR_BYTES,
};

struct UART_Server_Request {
	enum uart_server_request request_type;
	int uart_channel;
	char msg[800];
};

struct UART_Server_Request create_uart_request(enum uart_server_request request_type, const char* msg);

#endif