#include "uart.h"

int is_speed_byte(char byte) {
	return (1 <= byte && byte <= 31);
}

int is_train_byte(char byte) {
	return (byte == 1 || byte == 2 || byte == 24 || byte == 58 || byte == 74 || byte == 78);
}

int is_switch_direction(char byte) {
	return (byte == 33 || byte == 34);
}

int is_switch_number(char byte) {
	return ((byte <= 18 && byte >= 1) || (byte >= 153 && byte <= 156));
}
// TODO: one uart notifier can handle interrupts for both channels
// also, all the server names should be global constants
void uart_notifier() {
	int term_server_w = WhoIs(UART_SERVER_TERM_SEND);
	while (term_server_w == -1) {
		term_server_w = WhoIs(UART_SERVER_TERM_SEND);
	}

	int train_server_w = WhoIs(UART_SERVER_TRAIN_SEND);
	while (train_server_w == -1) {
		train_server_w = WhoIs(UART_SERVER_TRAIN_SEND);
	}

	int term_server_r = WhoIs(UART_SERVER_TERM_RECEIVE);
	while (term_server_r == -1) {
		term_server_r = WhoIs(UART_SERVER_TERM_RECEIVE);
	}

	int train_server_r = WhoIs(UART_SERVER_TRAIN_RECEIVE);
	while (train_server_r == -1) {
		train_server_r = WhoIs(UART_SERVER_TRAIN_RECEIVE);
	}
	char reply_buf[1];
	char msg[] = "N";
	struct UART_Server_Request request;

	for (;;) {
		int irq = AwaitEvent(145);
		ack_uart_interrupt();

		if (irq & (1 << 8)) { // channel 1
			irq &= 0xFF;
			if (irq == 194) {
				request = create_uart_request(UART_TX_TRIGGERED_1, msg);
				Send(train_server_w, (char*)&request, sizeof(struct UART_Server_Request), reply_buf, 1);
			} else if (irq == 204 || irq == 196) {
				request = create_uart_request(UART_RX_TRIGGERED_1, msg);
				Send(train_server_r, (char*)&request, sizeof(struct UART_Server_Request), reply_buf, 1);
			} else if (irq == 192 || irq == 224) {
				request = create_uart_request(UART_CTS_TRIGGERED, msg);
				Send(train_server_w, (char*)&request, sizeof(struct UART_Server_Request), reply_buf, 1);
			}
		} else { // channel 0
			if (irq == 194) {
				request = create_uart_request(UART_TX_TRIGGERED_0, msg);
				Send(term_server_w, (char*)&request, sizeof(struct UART_Server_Request), reply_buf, 1);
			} else if (irq == 204 || irq == 196) {
				request = create_uart_request(UART_RX_TRIGGERED_0, msg);
				Send(term_server_r, (char*)&request, sizeof(struct UART_Server_Request), reply_buf, 1);
			}
		}
	}
}

enum uart_tx_states {
	UART_TX_READY,
	UART_TXLO,
};

enum uart_train_tx_state {
	UART_TRAIN_TX_READY,
	UART_TRAIN_TXLO_CTS_0,
	UART_TRAIN_TXLO_CTS_1,
	UART_TRAIN_TXLO_CTS_2,
	UART_TRAIN_TXHI_CTS_0,
	UART_TRAIN_TXHI_CTS_1
};

enum uart_rx_states {
	UART_RXLO,
	UART_RX_READY,
};

enum uart_train_tx_state uart_train_tx_transition(enum uart_train_tx_state curstate,
												  enum uart_server_request interrupt) {

	if (curstate == UART_TRAIN_TXLO_CTS_0) {
		if (interrupt == UART_CTS_TRIGGERED) {
			return UART_TRAIN_TXLO_CTS_1;
		} else if (interrupt == UART_TX_TRIGGERED_1) {
			return UART_TRAIN_TXHI_CTS_0;
		} else {
			return UART_TRAIN_TXLO_CTS_0;
			ASSERT(0);
		}
	} else if (curstate == UART_TRAIN_TXLO_CTS_1) {
		if (interrupt == UART_CTS_TRIGGERED) {
			return UART_TRAIN_TXLO_CTS_2;
		} else if (interrupt == UART_TX_TRIGGERED_1) {
			return UART_TRAIN_TXHI_CTS_1;
		} else {
			return UART_TRAIN_TXLO_CTS_1;
			ASSERT(0);
		}
	} else if (curstate == UART_TRAIN_TXLO_CTS_2) {
		if (interrupt == UART_TX_TRIGGERED_1) {
			return UART_TRAIN_TX_READY;
		} else {
			return UART_TRAIN_TXLO_CTS_2;
			ASSERT(0);
		}
	} else if (curstate == UART_TRAIN_TXHI_CTS_0) {
		if (interrupt == UART_CTS_TRIGGERED) {
			return UART_TRAIN_TXHI_CTS_1;
		} else {
			return UART_TRAIN_TXHI_CTS_0;
			ASSERT(0);
		}

	} else if (curstate == UART_TRAIN_TXHI_CTS_1) {
		if (interrupt == UART_CTS_TRIGGERED) {
			return UART_TRAIN_TX_READY;
		} else {
			// TODO: FIGURE OUT WHY WE SOMETIMES GET DOUBLE TX INTERRUPTS
			ASSERT(interrupt == UART_TX_TRIGGERED_1);
			prints("additional tx interrupt\r\n");
			// ASSERT(0);
			return UART_TRAIN_TXHI_CTS_1;
		}
	} else if (curstate == UART_TRAIN_TX_READY) {
		debug_prints("\r\ninterrupt while ready\r\n");
		// ASSERT(interrupt == UART_CTS_TRIGGERED);
		// if (interrupt == UART_CTS_TRIGGERED) {
		// } else if (interrupt == UART_TX_TRIGGERED_1) {
		// 	prints("TX Interrupt while ready");
		// } else {
		// 	printi("Interrupt: ", interrupt, "\r\n");
		// 	ASSERT(0);
		// }
		return UART_TRAIN_TX_READY;
		// return UART_TRAIN_TXHI_CTS_1;
	} else {
		ASSERT(0);
	}
	return -1;
}

void uart_train_server_rx() {
	RegisterAs(UART_SERVER_TRAIN_RECEIVE);
	int sender;
	char rcvbuf[sizeof(struct UART_Server_Request)];
	char reply = 'R';

	struct Ring_Buffer char_buffer = create_buffer(CHAR);
	enum uart_rx_states rx_state = UART_RXLO;
	int sensor_reader_tid = 0;

	int bytes_read = 0;
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);

	for (;;) {
		enable_rx_interrupt(1);
		Receive(&sender, rcvbuf, sizeof(struct UART_Server_Request));
		struct UART_Server_Request* req = (struct UART_Server_Request*)rcvbuf;

		if (req->request_type == UART_GET_TRAIN) {
			if (!ring_buffer_is_empty(&char_buffer)) {
				reply = ring_buffer_pop(&char_buffer);
				Reply(sender, &reply, 1);
			} else {
				sensor_reader_tid = sender;
			}
		} else if (req->request_type == UART_RX_TRIGGERED_1) {
			rx_state = UART_RX_READY;
			Reply(sender, &reply, 1);
		} else {
			ASSERT(0);
		}

		// read as many as we can
		while (rx_state == UART_RX_READY && can_read_char(1)) {
			if (bytes_read == 0) {
				Delay(clock_server_tid, 4);
			}
			char c = UartGetc(1);
			bytes_read += 1;
			bytes_read %= 10;
			ASSERT(!ring_buffer_is_full(&char_buffer));
			ring_buffer_append(&char_buffer, c);
		}

		// if we got a char we can reply if the reader is waiting for us
		if (sensor_reader_tid != 0 && !ring_buffer_is_empty(&char_buffer)) {
			reply = ring_buffer_pop(&char_buffer);
			Reply(sensor_reader_tid, &reply, 1);
			sensor_reader_tid = 0;
		}

		rx_state = UART_RXLO;
	}
}

int is_directional_control(char byte) {
	return (byte == 'W' || byte == 'A' || byte == 'S' || byte == 'D');
}

void uart_term_server_rx() {
	RegisterAs(UART_SERVER_TERM_RECEIVE);
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	int sender;
	char rcvbuf[sizeof(struct UART_Server_Request)];
	char reply[] = "R";

	struct Ring_Buffer char_buffer = create_buffer(CHAR);
	enum uart_rx_states rx_state = UART_RXLO;

	struct Ring_Buffer terminal_admin_buffer = create_buffer(INT);
	uint32_t last_train_cmd_time = 0;

	char setup_terminal[] = "\033[55;80r"; // scrolling region for terminal and debug
	prints(setup_terminal);
	char prompt[] = "\033[51;1HCrocOs> ";
	prints(prompt);

	for (;;) {
		enable_rx_interrupt(0);
		Receive(&sender, rcvbuf, sizeof(struct UART_Server_Request));
		struct UART_Server_Request* req = (struct UART_Server_Request*)rcvbuf;

		if (req->request_type == UART_GET_TERM) {
			// only train admin should be sending this
			ring_buffer_append(&terminal_admin_buffer, sender);
		} else if (req->request_type == UART_RX_TRIGGERED_0) {
			rx_state = UART_RX_READY;
			Reply(sender, reply, 1);
		} else {
			ASSERT(0);
		}

		while (rx_state == UART_RX_READY && can_read_char(0)) {
			char c = UartGetc(0);
			if (is_directional_control(c) && ring_buffer_is_empty(&char_buffer)) {
				ASSERT(!ring_buffer_is_empty(&terminal_admin_buffer));
				uint32_t curtime = Time(clock_server_tid);
				if (last_train_cmd_time < curtime - 20) {
					last_train_cmd_time = curtime;
					int terminal_admin = ring_buffer_pop(&terminal_admin_buffer);
					char command[2] = { c, '\0' };
					Reply(terminal_admin, command, 2);
				}
			} else if (c == '\r') {
				ASSERT(!ring_buffer_is_empty(&terminal_admin_buffer));
				int terminal_admin = ring_buffer_pop(&terminal_admin_buffer);
				char command[100];
				int i = 0;
				while (!ring_buffer_is_empty(&char_buffer) && i < 99) {
					command[i] = (char)ring_buffer_pop(&char_buffer);
					i++;
				}
				command[i] = '\0';
				Reply(terminal_admin, command, 100);
				terminal_admin = 0;
			} else if (c == '\b') {
				if (!ring_buffer_is_empty(&char_buffer)) {
					ring_buffer_pop_back(&char_buffer);
					char msg[] = "\b \b";
					prints(msg);
				}
			} else {
				char msg[] = " ";
				msg[0] = c;
				prints(msg);
				ASSERT(!ring_buffer_is_full(&char_buffer));
				ring_buffer_append(&char_buffer, c);
			}
		}
		rx_state = UART_RXLO;
	}
}

// TODO: make replies async (at least to notifier)
void uart_term_server_tx() {
	RegisterAs(UART_SERVER_TERM_SEND);
	int sender;
	char rcvbuf[sizeof(struct UART_Server_Request)];
	char reply = 'R';

	struct Ring_Buffer char_buffer_0 = create_buffer(CHAR);
	enum uart_tx_states tx_state = UART_TX_READY;

	// check status → action or wait for interrupt → repeat
	for (;;) {
		Receive(&sender, rcvbuf, sizeof(struct UART_Server_Request));
		struct UART_Server_Request* req = (struct UART_Server_Request*)rcvbuf;
		if (req->request_type == UART_SEND_TERM) {
			int i = 0;
			while (req->msg[i] != '\0') {
				ASSERT(!ring_buffer_is_full(&char_buffer_0));
				ring_buffer_append(&char_buffer_0, req->msg[i]);
				i++;
			}
		} else if (req->request_type == UART_TX_TRIGGERED_0) {
			// no cts, this means we can send
			tx_state = UART_TX_READY;
		} else {
			ASSERT(0);
		}
		Reply(sender, &reply, sizeof(char));

		// check status and handle
		char byte;
		while (tx_state == UART_TX_READY && can_send_char(0) && !ring_buffer_is_empty(&char_buffer_0)) {
			byte = (char)ring_buffer_pop(&char_buffer_0);
			UartPutc(0, 0, byte);
		}
		if (!ring_buffer_is_empty(&char_buffer_0)) {
			tx_state = UART_TXLO;
			enable_tx_interrupt(0);
		}
	}
}

void uart_train_server_tx() {
	RegisterAs(UART_SERVER_TRAIN_SEND);
	int sender;
	char rcvbuf[sizeof(struct UART_Server_Request)];
	char reply = 'R';

	struct Ring_Buffer char_buffer_1 = create_buffer(CHAR);
	enum uart_train_tx_state tx_state = UART_TRAIN_TX_READY;

	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	uint32_t last_byte_sent_time = 0;
	uint32_t curtime = 0;
	// check status → action or wait for interrupt → repeat
	for (;;) {
		curtime = Time(clock_server_tid);
		Receive(&sender, rcvbuf, sizeof(struct UART_Server_Request));
		struct UART_Server_Request* req = (struct UART_Server_Request*)rcvbuf;

		if (last_byte_sent_time != 0 && curtime - last_byte_sent_time > 100) {
			debug_printi("Error, more than 1s delay to train set, current state: ", tx_state, "\r\n");
			debug_prints("Going back to ready state...");
			tx_state = UART_TRAIN_TX_READY;
		}

		if (req->request_type == UART_SEND_TRAIN) {
			int i = 0;
			// TODO: we won't be able to send byte 0 to the train like this
			// might be better to have an explicit length field in the request
			while (req->msg[i] != '\0') {
				ASSERT(!ring_buffer_is_full(&char_buffer_1));
				if (req->msg[i] == 16 && is_train_byte(req->msg[i + 1])) {
					i++; // put train num on first
					ring_buffer_insert_front(&char_buffer_1, req->msg[i]);
					ring_buffer_insert_front(&char_buffer_1, 16);
					debug_printi("size of buffer when stop bytes added: ", char_buffer_1.size, "\r\n");
					debug_printi("added stop byte to queue at time: ", Time(clock_server_tid), "\r\n");
				} else {
					ring_buffer_append(&char_buffer_1, req->msg[i]);
				}

				i++;
			}
		} else if (req->request_type == UART_TX_TRIGGERED_1 || req->request_type == UART_CTS_TRIGGERED) {
			tx_state = uart_train_tx_transition(tx_state, req->request_type);
		} else {
			ASSERT(0);
		}

		Reply(sender, &reply, sizeof(char));

		// check status and handle
		char byte;
		if (tx_state == UART_TRAIN_TX_READY && can_send_char(1) && !ring_buffer_is_empty(&char_buffer_1)) {
			byte = (char)ring_buffer_pop(&char_buffer_1);
			if (byte == 16) {
				debug_printi("sending stop byte 1 to train: at time: ", Time(clock_server_tid), "\r\n");
			}
			UartPutc(0, 1, byte);
			last_byte_sent_time = curtime;
			tx_state = UART_TRAIN_TXLO_CTS_0;
			enable_tx_interrupt(1);
			enable_modem_interrupt(1);
		}
		if (tx_state == UART_TRAIN_TXLO_CTS_1 || tx_state == UART_TRAIN_TXHI_CTS_1) {
			enable_modem_interrupt(1);
		}

		if (tx_state == UART_TRAIN_TXHI_CTS_1 || tx_state == UART_TRAIN_TXHI_CTS_0) {
			ASSERT(!(UartReadRegister(1, 1) & 2));
		}
	}
}

// this task waits the given number of ticks and then sends to the train tx uart server to ready it, for use when there
// is no CTS signal
void waiting_task() {
	int sender_tid;
	char rcv_buf;
	Receive(&sender_tid, &rcv_buf, 1);
	Reply(sender_tid, &rcv_buf, 1);

	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	struct UART_Server_Request req = create_uart_request(UART_TX_GO_BACK_TO_READY, "");
	Delay(clock_server_tid, (int)rcv_buf);
	// assume the sender is the tx uart server
	Send(get_tid_by_name(UART_SERVER_TRAIN_SEND), (char*)&req, sizeof(struct UART_Server_Request), &rcv_buf, 1);
	Exit();
}

char ticks_to_wait_before_sending(char last_command) {
	// fn assumes that we just finished sending the entire command, handle the case of waiting b/n bytes of a command
	// separately

	// train command is speed (1-16), then number (1, 2, 24, 58, 74, 78)
	// switch command is direction (33, 34), then switch number (1-18, 153-156)
	// solenoid off is 32
	// sensor query is 133

	// Train commands generally have a 15-17ms CTS gap, i.e., 20ms wait time should be fine.
	// A first turnout command results in a similar CTS gap. -> wait 20ms
	// Immediately following turnout commands have a CTS gap of ~140ms each! -> wait 150ms
	// The final 0x20 turnout off command has a CTS gap of ~55ms. -> wait 60ms

	if (is_switch_number(last_command)) {
		// switch command, max wait time is 150
		// this can be optimized, 150ms is fine for now
		return 15;
	} else if (last_command == 1 || last_command == 2 || last_command == 24 || last_command == 58 || last_command == 74
			   || last_command == 78) {
		// speed command
		return 2;
	} else if (last_command == 32) {
		// solenoid off
		return 6;
	} else if (last_command == 133 || last_command == 192) {
		return 0;
	}
	debug_printi("got unexpected command, ", last_command, "\r\n");
	return 2;
}

void uart_train_server_tx_no_cts() {
	RegisterAs(UART_SERVER_TRAIN_SEND);
	int sender;
	char rcvbuf[sizeof(struct UART_Server_Request)];
	char reply = 'R';

	struct Ring_Buffer char_buffer_1 = create_buffer(CHAR);
	enum uart_train_tx_state tx_state = UART_TRAIN_TX_READY;

	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	uint32_t last_byte_sent_time = 0; // time in ticks
	char wait_time = 0;				  // time to wait in ticks
	uint32_t curtime = 0;
	int waiting_task_id;
	int receiving_sensor_bytes = 0;

	uint32_t next_wait_time = 0;
	// check status → action or wait for interrupt → repeat
	for (;;) {
		Receive(&sender, rcvbuf, sizeof(struct UART_Server_Request));
		curtime = Time(clock_server_tid);
		struct UART_Server_Request* req = (struct UART_Server_Request*)rcvbuf;

		if (last_byte_sent_time != 0 && curtime - last_byte_sent_time > 500) {
			debug_printi("Current receiving_sensor_bytes: ", receiving_sensor_bytes, "\r\n");
			debug_printi("Error, more than 5s delay to train set, current state: ", tx_state, "\r\n");
			debug_prints("Going back to ready state...\r\n");
			// ASSERT(0);
			tx_state = UART_TRAIN_TX_READY;
			receiving_sensor_bytes = 0;
		}

		if (req->request_type == UART_SEND_TRAIN) {
			int i = 0;
			// TODO: we won't be able to send byte 0 to the train like this
			// might be better to have an explicit length field in the request
			while (req->msg[i] != '\0') {
				ASSERT(!ring_buffer_is_full(&char_buffer_1));
				if (req->msg[i] == 16 && is_train_byte(req->msg[i + 1])) {
					i++; // put train num on first
					ring_buffer_insert_front(&char_buffer_1, req->msg[i]);
					ring_buffer_insert_front(&char_buffer_1, 16);
					debug_printi("size of buffer when stop bytes added: ", char_buffer_1.size, "\r\n");
					debug_printi("added stop byte to queue at time: ", curtime, "\r\n");
				} else {
					if (req->msg[i] == 133 && receiving_sensor_bytes) {
						i++;
						continue;
					}
					ring_buffer_append(&char_buffer_1, req->msg[i]);
				}

				i++;
			}
		} else if (req->request_type == UART_TX_TRIGGERED_1) {
			// only tx enabled, count it
			if (tx_state == UART_TRAIN_TXLO_CTS_0) {
				tx_state = UART_TRAIN_TXHI_CTS_0;
			} else if (tx_state == UART_TRAIN_TXLO_CTS_1) {
				tx_state = UART_TRAIN_TX_READY;
			} else {
				debug_prints("spurious tx interrupt\r\n");
				debug_printi("incoming request type: ", req->request_type, "\r\n");
				debug_printi("current tx state: ", tx_state, "\r\n");
			}
		} else if (req->request_type == UART_TX_GO_BACK_TO_READY) {
			if (tx_state == UART_TRAIN_TXLO_CTS_0) {
				tx_state = UART_TRAIN_TXLO_CTS_1;
			} else if (tx_state == UART_TRAIN_TXHI_CTS_0) {
				// debug_printi("back to ready at time ", curtime, "\r\n");
				tx_state = UART_TRAIN_TX_READY;
			} else {
				debug_printi("incoming request type: ", req->request_type, "\r\n");
				debug_printi("current tx state: ", tx_state, "\r\n");
				ASSERT(0);
			}
		} else if (req->request_type == UART_DONE_RECEIVING_SENSOR_BYTES) {
			receiving_sensor_bytes = 0;
		} else {
			ASSERT(0);
		}

		Reply(sender, &reply, sizeof(char));

		// check status and handle
		char byte;
		if (tx_state == UART_TRAIN_TX_READY && can_send_char(1) && !ring_buffer_is_empty(&char_buffer_1)
			&& !receiving_sensor_bytes) {
			// we want to be able to reset the sensor query byte
			// if (char_buffer_1.cbuffer[char_buffer_1.l] != 133 && receiving_sensor_bytes) {
			// // cant send yet, wait until we get all sensor bytes
			// continue;
			// }

			byte = ring_buffer_pop(&char_buffer_1);
			last_byte_sent_time = curtime;
			tx_state = UART_TRAIN_TXLO_CTS_0;
			UartPutc(0, 1, byte);

			if (byte == 133) {
				receiving_sensor_bytes = 1;
			} else {
				debug_printi("Send byte: ", byte, " to train\r\n");
			}

			// determine waiting time for no cts
			if (is_speed_byte(byte) && !ring_buffer_is_empty(&char_buffer_1)
				&& is_train_byte(char_buffer_1.cbuffer[char_buffer_1.l])) {
				// just sent first byte of train command
				wait_time = 0;
				next_wait_time = 4;
			} else if (is_switch_direction(byte) && !ring_buffer_is_empty(&char_buffer_1)
					   && is_switch_number(char_buffer_1.cbuffer[char_buffer_1.l])) {
				wait_time = 0;
				next_wait_time = 17;
			} else if (byte == 32) {
				wait_time = 8;
			} else {
				wait_time = next_wait_time;
				next_wait_time = 0;
			}

			if (wait_time != 0) {
				waiting_task_id = Create(1, waiting_task);
				Send(waiting_task_id, &wait_time, 1, &wait_time, 1);
			} else {
				tx_state = UART_TRAIN_TXLO_CTS_1;
			}
			enable_tx_interrupt(1);
		}
	}
}