#include "term_admin.h"
static uint8_t TRACK_INITIALIZED = 1; // we did not zero bss so global bools are | 1: False, 2:True | :^)
const int MAX_TOKENS = 10;

struct Command_Token get_next_token(char* command, int* offset) {
	struct Command_Token token;
	while (command[(*offset)] == ' ')
		(*offset)++;
	if (command[(*offset)] == '\0') {
		token.type = COMMAND_EOL;
	} else if (command[(*offset)] == 't' && command[(*offset) + 1] == 'r') {
		token.type = COMMAND_TR;
		*offset += 2;
	} else if (command[(*offset)] == 'r' && command[(*offset) + 1] == 'v' && command[(*offset) + 2] == 'i') {
		token.type = COMMAND_RV_I;
		*offset += 3;
	} else if (command[(*offset)] == 'r' && command[(*offset) + 1] == 'v') {
		token.type = COMMAND_RV;
		*offset += 2;
	} else if (command[*offset] == 's' && command[*offset + 1] == 'w') {
		token.type = COMMAND_SW;
		*offset += 2;
	} else if (command[*offset] == 'i' && command[*offset + 1] == 'n' && command[*offset + 2] == 'i'
			   && command[*offset + 3] == 't') {
		token.type = COMMAND_INIT_TRACK;
		*offset += 4;
	} else if (command[*offset] == 'p' && command[*offset + 1] == 'a' && command[*offset + 2] == 't'
			   && command[*offset + 3] == 'h') {
		token.type = COMMAND_PATHFIND;
		*offset += 4;
	} else if (command[*offset] == 'c' && command[*offset + 1] == 'a' && command[*offset + 2] == 'l') {
		token.type = COMMAND_CALIBRATE;
		*offset += 3;
	} else if (command[*offset] == 's' && command[*offset + 1] == 'e' && command[*offset + 2] == 't') {
		token.type = COMMAND_SET_TRAIN;
		*offset += 3;
	} else if (command[*offset] == 's' && command[*offset + 1] == 't' && command[*offset + 2] == 'o'
			   && command[*offset + 3] == 'p') {
		token.type = COMMAND_LOOP_STOP;
		*offset += 4;
	} else if (command[*offset] == 'r' && command[*offset + 1] == 'n' && command[*offset + 2] == 'g') {
		token.type = COMMAND_RNG;
		*offset += 3;
	} else if (command[*offset] == 'l' && command[*offset + 1] == 'o' && command[*offset + 2] == 'c') {
		token.type = COMMAND_LOCATE;
		*offset += 3;
	} else if (is_alphabetical(command[*offset])) {
		token.type = COMMAND_CHAR;
		token.value = command[*offset];
		*offset += 1;
	} else if (is_number(command[*offset])) {
		token.type = COMMAND_INT;
		token.value = parse_possible_three_digit_int(command + *offset, offset); // updates offset for us
	} else {
		token.type = COMMAND_INVALID_TOKEN;
	}
	return token;
}

void parse_command(char* command, struct Command_Token* parsed_cmds) {
	int offset = 0;
	for (int i = 0; i < MAX_TOKENS; i++) {
		parsed_cmds[i] = get_next_token(command, &offset);
		if (parsed_cmds[i].type == COMMAND_EOL || parsed_cmds[i].type == COMMAND_INVALID_TOKEN) {
			break;
		}
	}
}

void terminal_admin() {
	int terminal_rx_server_tid = get_tid_by_name(UART_SERVER_TERM_RECEIVE);
	char command[100];
	struct Command_Token tokens[MAX_TOKENS];
	char prompt[] = "\033[51;1H\033[KCrocOs> ";

	for (;;) {
		GetNextTerminalCommand(terminal_rx_server_tid, command);
		parse_command(command, tokens);

		if (TRACK_INITIALIZED == 1 && tokens[0].type != COMMAND_INIT_TRACK) {
			debug_prints("Track must be initialized before commands can be sent\r\nUsage: init [TRACK]\r\n");
			prints(prompt);
			continue;
		}

		if (tokens[0].type == COMMAND_TR && tokens[1].type == COMMAND_INT && tokens[2].type == COMMAND_INT
			&& tokens[3].type == COMMAND_EOL) {
			send_speed_command(tokens[1].value, tokens[2].value);
		} else if (tokens[0].type == COMMAND_RV && tokens[1].type == COMMAND_INT && tokens[2].type == COMMAND_EOL) {
			send_reverse_command(tokens[1].value);
		} else if (tokens[0].type == COMMAND_RV_I && tokens[1].type == COMMAND_INT && tokens[2].type == COMMAND_EOL) {
			reverse_instantly(tokens[1].value);
		} else if (tokens[0].type == COMMAND_SW && tokens[1].type == COMMAND_INT && tokens[2].type == COMMAND_CHAR
				   && tokens[3].type == COMMAND_EOL) {
			char switch_num[1] = { tokens[1].value };
			char direction[1] = { tokens[2].value };
			turn_switches(switch_num, direction, 1);
		} else if (tokens[0].type == COMMAND_INIT_TRACK && tokens[1].type == COMMAND_CHAR
				   && tokens[2].type == COMMAND_EOL) {
			init_track(tokens[1].value);
			TRACK_INITIALIZED = 2;
		} else if (tokens[0].type == COMMAND_CHAR && tokens[0].value == 'q' && tokens[1].type == COMMAND_EOL) {
			Quit();
		} else if (tokens[0].type == COMMAND_PATHFIND && tokens[1].type == COMMAND_INT && tokens[2].type == COMMAND_CHAR
				   && tokens[3].type == COMMAND_INT && tokens[4].type == COMMAND_INT && tokens[5].type == COMMAND_EOL) {
			uint8_t dest_sensor = (tokens[2].value - 'A') * 16 + tokens[3].value - 1;
			uint8_t offset = tokens[4].value;
			set_path2(tokens[1].value, dest_sensor, offset);
		} else if (tokens[0].type == COMMAND_CALIBRATE && tokens[1].type == COMMAND_INT) {
			calibrate_train(tokens[1].value);
		} else if (tokens[0].type == COMMAND_SET_TRAIN && tokens[1].type == COMMAND_INT) {
			set_cur_train(tokens[1].value);
		} else if (tokens[0].type == COMMAND_LOOP_STOP && tokens[1].type == COMMAND_CHAR
				   && tokens[2].type == COMMAND_INT) {
			uint8_t dest_sensor = (tokens[1].value - 'A') * 16 + tokens[2].value - 1;
			loop_stop(dest_sensor);
		} else if (tokens[0].type == COMMAND_RNG && tokens[1].type == COMMAND_EOL) {
			toggle_random_paths();
		} else if (tokens[0].type == COMMAND_LOCATE && tokens[1].type == COMMAND_INT && tokens[2].type == COMMAND_CHAR
				   && tokens[3].type == COMMAND_INT && tokens[4].type == COMMAND_EOL) {
			int train_num = tokens[1].value;
			uint8_t sensor = (tokens[2].value - 'A') * 16 + tokens[3].value - 1;
			locate_train(train_num, sensor);
		} else if (tokens[0].type == COMMAND_CHAR && tokens[1].type == COMMAND_EOL
				   && (tokens[0].value == 'W' || tokens[0].value == 'A' || tokens[0].value == 'S'
					   || tokens[0].value == 'D')) {
			control_user_train(tokens[0].value);
		} else {
			debug_prints("Invalid Command\r\n");
		}
		prints(prompt);
	}
}