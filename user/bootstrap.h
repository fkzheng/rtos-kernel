#ifndef USER_TASKS_H
#define USER_TASKS_H
#include "clock.h"
#include "kernel/task.h"
#include "pathfinding.h"
#include "random.h"
#include "request.h"
#include "sensors.h"
#include "servers.h"
#include "syscall.h"
#include "track_admin.h"
#include "train_control.h"
#include "uart.h"
#include "util/ring_buffer.h"
#include "util/util.h"

void bootstrap_k4();
void bootstrap_tc1();

#endif