#include "train_tests.h"

void print_path(char* prefix, struct Ring_Buffer* buf) {
	uint8_t node;
	char final_msg[(7 * buf->size) + 2 + charsize(prefix)];
	str_copy(prefix, final_msg);
	for (int i = 0; i < buf->size; i++) {
		node = buf->cbuffer[buf->l + i % g_max_cbuffer_elements];
		str_cat(final_msg, (char*)track_nodes[node].name);
		if (i != buf->size - 1) {
			str_cat(final_msg, "->");
		}
	}
	str_cat(final_msg, "\r\n");
	debug_prints(final_msg);
}

void test_dijkstra(int src, int dest) {
	prints("\0337\033[40;1HTESTING\r\n");
	struct Ring_Buffer path = create_buffer(CHAR);
	dijkstra(src, dest, &path);
	print_path("", &path);
	prints("\0338");
}
