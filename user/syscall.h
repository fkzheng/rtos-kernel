#ifndef SYSCALL_H
#define SYSCALL_H
#include "bootstrap.h" // gives us name_server_tid
#include "request.h"

struct Syscall_Request;
extern int kerent(struct Syscall_Request* request);

// user syscalls
int Create(int priority, void (*function)());
int MyTid();
int MyParentTid();
void Yield();
uint32_t Idle();
void Exit();
int Send(int tid, const char* msg, int msglen, char* reply, int replylen);
int Receive(int* tid, char* msg, int msglen);
int Reply(int tid, void* reply, int replylen);

int Unregister();
int RegisterAs(const char* name);
int WhoIs(const char* name);

int AwaitEvent(int event_id);

int UpdateTime(int tid, uint32_t cur_time);
int Time(int tid);
int Delay(int tid, int ticks);
int DelayUntil(int tid, int ticks);

int UartWriteRegister(int channel, char reg, char data);
int UartReadRegister(int channel, char reg);

int Getc(int tid, int uart_channel);
int Puts(int tid, int uart_channel, const char* msg);
int Putc(int tid, int uart_channel, char c);
void GetNextTerminalCommand(int terminal_rx_server_tid, char* buf);

int GetIdleTime();

void Quit();
#endif