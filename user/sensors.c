#include "sensors.h"

int parse_sensors(struct Ring_Buffer* recent_sensors,
				  char* sensor_states,
				  int sensor_num,
				  char sensor_byte1,
				  char sensor_byte2,
				  int* ret) {
	int changed = 0;
	int s88_id = sensor_num * SENSOR_MAX_NUMBER;
	int sensor_encoding1, sensor_encoding2;
	int i;
	for (i = 0; i < 8; i++) {
		// byte 1: sensors 1-8
		// byte 2: sensors 9-16
		sensor_encoding1 = s88_id + 7 - i;
		sensor_encoding2 = s88_id + 15 - i;
		if (sensor_byte1 & (1 << i) && sensor_states[sensor_encoding1] == 0) {
			if (recent_sensors->size >= SENSOR_MAX_QUEUE_SIZE) {
				ring_buffer_pop(recent_sensors);
			}
			ASSERT(!ring_buffer_is_full(recent_sensors));
			ring_buffer_append(recent_sensors, sensor_encoding1);
			sensor_states[sensor_encoding1] = 1;
			ret[changed] = sensor_encoding1;
			changed++;
		} else if (!(sensor_byte1 & (1 << i))) {
			sensor_states[sensor_encoding1] = 0;
		}

		if (sensor_byte2 & (1 << i) && sensor_states[sensor_encoding2] == 0) {
			if (recent_sensors->size >= SENSOR_MAX_QUEUE_SIZE) {
				ring_buffer_pop(recent_sensors);
			}
			ASSERT(!ring_buffer_is_full(recent_sensors));
			ring_buffer_append(recent_sensors, sensor_encoding2);
			sensor_states[sensor_encoding2] = 1;
			ret[changed] = sensor_encoding2;
			changed++;
		} else if (!(sensor_byte2 & (1 << i))) {
			sensor_states[sensor_encoding2] = 0;
		}
	}
	return changed;
}

void print_sensors(struct Ring_Buffer* recent_sensors) {
	ASSERT(!ring_buffer_is_empty(recent_sensors));
	int front = recent_sensors->l;
	int rear = recent_sensors->r;
	int encoded_sensor, s88_id, sensor_number;

	// save the cursor, jump to location on terminal, set color cyan :)
	char prefix[] = "\0337\033[4;1H\033[36m";
	// Reset special formatting, reload the cursor to original position
	char suffix[] = "\033[0m\0338";
	char sensor_out[] = "XXX ";
	char out_msg[sizeof(prefix) + sizeof(suffix) + (sizeof(sensor_out) * 10) + 1];
	str_copy(prefix, out_msg);

	// read the queue backwards for most recent sensors first
	while (rear != front) {
		rear = (rear > 0) ? (rear - 1) : g_max_ibuffer_elements - 1;

		encoded_sensor = recent_sensors->ibuffer[rear];
		s88_id = encoded_sensor / 16 + 'A';
		sensor_number = encoded_sensor % 16 + 1;

		sensor_out[0] = s88_id;
		sensor_out[1] = sensor_number / 10 + '0';
		sensor_out[2] = sensor_number % 10 + '0';

		str_cat(out_msg, sensor_out);
	}

	str_cat(out_msg, suffix);
	prints(out_msg);
}

void print_sensor_prediction_results(int sensor_error, int distance_error) {
	char prefix[] = "\0337\033[18;1H\033[K\033[36mTime off: ";
	char suffix[] = " ticks\033[0m\0338";
	printi_maybe_negative(prefix, sensor_error, suffix);
	char prefix_2[] = "\0337\033[19;1H\033[K\033[36mDist off: ";
	char suffix_2[] = " mm\033[0m\0338";
	printi_maybe_negative(prefix_2, distance_error, suffix_2);
}

void update_train_server(int tid, uint8_t sensor_id) {
	char replybuf[1];
	struct Train_Admin_Request req = { .request_type = TA_SENSOR_UPDATE, .update = sensor_id };
	Send(tid, (char*)&req, sizeof(struct Train_Admin_Request), replybuf, 1);
}

void sensor_read_courier() {
	int train_rx_tid = get_tid_by_name(UART_SERVER_TRAIN_RECEIVE);
	int train_admin_tid = get_tid_by_name(TRAIN_ADMIN);

	// int train_tx_tid = get_tid_by_name(UART_SERVER_TRAIN_SEND);
	// int clock_server_tid = get_tid_by_name(CLOCK_SERVER);

	struct Ring_Buffer recent_sensors = create_buffer(INT);
	// keep track of sensor states to dedup trains holding down a sensor
	// we need to get a read without the sensor triggered before processing it again
	// **Unlikely** edge case where 2 trains hit the same sensor within 200ms of eachother
	// will break this... Fix by increasing polling interval
	char sensor_states[SENSOR_COUNT * SENSOR_MAX_NUMBER] = { 0 };
	int sensor_updates[6]; // theoretical max number of sensors that can trigger at same time

	char sensor_byte1, sensor_byte2;
	int sensor_num = 0;
	// char reply;
	// struct UART_Server_Request done_reading_req = create_uart_request(UART_DONE_RECEIVING_SENSOR_BYTES, "");
	// uint32_t last_sensor_finish_read_time = 0;
	// uint32_t curtime;
	for (;;) {
		sensor_byte1 = Getc(train_rx_tid, 1);
		sensor_byte2 = Getc(train_rx_tid, 1);
		// We only care to update the recent sensors on the screen if we processed a sensor read
		int updates
			= parse_sensors(&recent_sensors, sensor_states, sensor_num, sensor_byte1, sensor_byte2, sensor_updates);
		if (updates) {
			for (int i = 0; i < updates; i++) {
				update_train_server(train_admin_tid, sensor_updates[i]);
			}
			print_sensors(&recent_sensors);
		}

		sensor_num = (sensor_num + 1) % SENSOR_COUNT;
		// if (sensor_num == 0) {
		// 	curtime = Time(clock_server_tid);
		// 	if (curtime - last_sensor_finish_read_time > SENSOR_QUERY_INTERVAL) {
		// 		debug_printi("took ",
		// 					 (curtime - last_sensor_finish_read_time) * 10,
		// 					 " ms to get all sensors back since last time we got all sensors\r\n");
		// 	}
		// 	last_sensor_finish_read_time = curtime;
		// 	// uncomment this line when CTS is broken
		// 	// Send(train_tx_tid, (char*)&done_reading_req, sizeof(struct UART_Server_Request), &reply, 1);
		// }
	}

	ASSERT(0);
	Exit();
}

void sensor_read_courier_no_cts() {
	int train_rx_tid = get_tid_by_name(UART_SERVER_TRAIN_RECEIVE);
	int train_admin_tid = get_tid_by_name(TRAIN_ADMIN);

	int train_tx_tid = get_tid_by_name(UART_SERVER_TRAIN_SEND);
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);

	struct Ring_Buffer recent_sensors = create_buffer(INT);
	// keep track of sensor states to dedup trains holding down a sensor
	// we need to get a read without the sensor triggered before processing it again
	// **Unlikely** edge case where 2 trains hit the same sensor within 200ms of eachother
	// will break this... Fix by increasing polling interval
	char sensor_states[SENSOR_COUNT * SENSOR_MAX_NUMBER] = { 0 };
	int sensor_updates[6]; // theoretical max number of sensors that can trigger at same time

	char sensor_byte1, sensor_byte2;
	int sensor_num = 0;
	char reply;
	struct UART_Server_Request done_reading_req = create_uart_request(UART_DONE_RECEIVING_SENSOR_BYTES, "");
	uint32_t last_sensor_finish_read_time = 0;
	uint32_t curtime;
	for (;;) {
		sensor_byte1 = Getc(train_rx_tid, 1);
		sensor_byte2 = Getc(train_rx_tid, 1);
		// We only care to update the recent sensors on the screen if we processed a sensor read
		int updates
			= parse_sensors(&recent_sensors, sensor_states, sensor_num, sensor_byte1, sensor_byte2, sensor_updates);
		if (updates) {
			for (int i = 0; i < updates; i++) {
				update_train_server(train_admin_tid, sensor_updates[i]);
			}
			print_sensors(&recent_sensors);
		}

		sensor_num = (sensor_num + 1) % SENSOR_COUNT;
		if (sensor_num == 0) {
			curtime = Time(clock_server_tid);
			if (curtime - last_sensor_finish_read_time > SENSOR_QUERY_INTERVAL * 2) {
				debug_printi("took ",
							 (curtime - last_sensor_finish_read_time) * 10,
							 " ms to get all sensors back since last time we got all sensors\r\n");
			}
			last_sensor_finish_read_time = curtime;
			Send(train_tx_tid, (char*)&done_reading_req, sizeof(struct UART_Server_Request), &reply, 1);
		}
	}

	ASSERT(0);
	Exit();
}

void sensor_query_server() {
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	int train_tx_tid = get_tid_by_name(UART_SERVER_TRAIN_SEND);

	// Configures sensor to reset after dump
	char reset = 192;

	// Send first sensor query
	char query = 133;

	Putc(train_tx_tid, 1, reset);
	Delay(clock_server_tid, 200);
	Putc(train_tx_tid, 1, query);

	// number of 10ms ticks since start
	uint32_t target_time = Time(clock_server_tid);
	for (;;) {
		target_time += SENSOR_QUERY_INTERVAL;
		DelayUntil(clock_server_tid, target_time);

		// query sensors
		Putc(train_tx_tid, 1, query);
	}

	ASSERT(0);
	Exit();
}
