#include "pathfinding.h"

uint8_t minDistance(struct track_path* paths, uint8_t* sptSet) {
	// Initialize min value
	uint16_t min_dist = UINT16_MAX;
	uint8_t min_index = 0;

	for (int v = 0; v < TRACK_NODE_COUNT; v++) {
		if (!sptSet[v] && paths[v].dist <= min_dist) {
			min_dist = paths[v].dist;
			min_index = v;
		}
	}

	return min_index;
}

void getPath(struct track_path path, char* buf) {
	str_copy("|", buf);
	while (path.prev) {
		str_cat(buf, "<-");
		str_cat(buf, (char*)path.cur->name);
		path = *path.prev;
	}
}

int node_distance(uint8_t src, uint8_t dest) {
	struct Ring_Buffer unused = create_buffer(CHAR);
	return dijkstra(src, dest, &unused);
}

// 8ms when it's the only task performing real work ?
int dijkstra(uint8_t src, uint8_t dest, struct Ring_Buffer* path_buf) {
	// uint32_t starttime, endtime;
	// starttime = get_true_timestamp();
	struct track_path paths[TRACK_NODE_COUNT];
	uint8_t sptSet[TRACK_NODE_COUNT];

	// initialize starting values
	for (int i = 0; i < TRACK_NODE_COUNT; i++) {
		paths[i].dist = UINT16_MAX;
		paths[i].cur = &track_nodes[i];
		paths[i].prev = NULL;
		sptSet[i] = 0;
	}

	paths[src].dist = 0;

	uint8_t next_S, next_C, next_R;
	uint16_t edge_len_S, edge_len_C, edge_len_R;
	for (int count = 0; count < TRACK_NODE_COUNT - 1; count++) {
		uint8_t u = minDistance(paths, sptSet);
		struct track_node cur_node = track_nodes[u];
		sptSet[u] = 1;

		if (paths[u].dist == UINT16_MAX) {
			break;
		}
		switch (cur_node.type) {
		case NODE_SENSOR:
			next_S = cur_node.edge[DIR_AHEAD].dest->idx;
			edge_len_S = cur_node.edge[DIR_AHEAD].dist;
			if (!sptSet[next_S] && paths[u].dist + edge_len_S < paths[next_S].dist) {
				paths[next_S].dist = paths[u].dist + edge_len_S;
				paths[next_S].prev = &paths[u];
			}
			// reverse node

			next_R = reverse_node(u);
			edge_len_R = 0;
			if (!sptSet[next_R] && paths[u].dist + edge_len_R < paths[next_R].dist) {
				paths[next_R].dist = paths[u].dist + edge_len_R;
				paths[next_R].prev = &paths[u];
			}
			break;
		case NODE_BRANCH:
			next_S = cur_node.edge[DIR_STRAIGHT].dest->idx;
			next_C = cur_node.edge[DIR_CURVED].dest->idx;
			edge_len_S = cur_node.edge[DIR_STRAIGHT].dist;
			edge_len_C = cur_node.edge[DIR_CURVED].dist;
			if (!sptSet[next_S] && paths[u].dist + edge_len_S < paths[next_S].dist) {
				paths[next_S].dist = paths[u].dist + edge_len_S;
				paths[next_S].prev = &paths[u];
			}
			if (!sptSet[next_C] && paths[u].dist + edge_len_C < paths[next_C].dist) {
				paths[next_C].dist = paths[u].dist + edge_len_C;
				paths[next_C].prev = &paths[u];
			}
			break;
		case NODE_MERGE:
			next_S = cur_node.edge[DIR_AHEAD].dest->idx;
			edge_len_S = cur_node.edge[DIR_AHEAD].dist;
			if (!sptSet[next_S] && paths[u].dist + edge_len_S < paths[next_S].dist) {
				paths[next_S].dist = paths[u].dist + edge_len_S;
				paths[next_S].prev = &paths[u];
			}
			break;
		case NODE_ENTER:
			next_S = cur_node.edge[DIR_AHEAD].dest->idx;
			edge_len_S = cur_node.edge[DIR_AHEAD].dist;
			if (!sptSet[next_S] && paths[u].dist + edge_len_S < paths[next_S].dist) {
				paths[next_S].dist = paths[u].dist + edge_len_S;
				paths[next_S].prev = &paths[u];
			}
			break;
		case NODE_EXIT:
			break;
		case NODE_NONE:
			next_S = cur_node.edge[DIR_AHEAD].dest->idx;
			edge_len_S = cur_node.edge[DIR_AHEAD].dist;
			if (!sptSet[next_S] && paths[u].dist + edge_len_S < paths[next_S].dist) {
				paths[next_S].dist = paths[u].dist + edge_len_S;
				paths[next_S].prev = &paths[u];
			}
			break;
		default:
			ASSERT(0);
			break;
		}
	}

	struct track_path path = paths[dest];
	uint8_t tmp[50];
	uint8_t count = 0;
	while (path.prev) {
		tmp[count] = path.cur->idx;
		count++;
		path = *path.prev;
	}
	// add first node
	tmp[count] = path.cur->idx;
	count++;

	for (int i = 0; i < count; i++) {
		ring_buffer_append(path_buf, tmp[count - i - 1]);
	}

	return paths[dest].dist;
}

// check that this section is not blocked, we are travelling through it
int is_section_blocked(
	uint16_t velocity, uint32_t blocked[144][6][2], int node_idx, uint32_t dist, uint32_t start_time) {
	uint32_t time_to_loc = ((dist * 100) / velocity) + start_time;
	uint32_t time_to_loc_min = time_to_loc < 100 ? 0 : time_to_loc - 100; // - 1 second for error
	uint32_t time_to_loc_max = time_to_loc + 400;						  // + 4 second for error
	for (int i = 0; i < 6; i++) {
		// debug_printi("time to node ", node_idx, "\r\n");
		if (time_to_loc_min <= blocked[node_idx][i][1] && blocked[node_idx][i][0] <= time_to_loc_max) {
			char out[40];
			str_copy("BLOCKED ", out);
			str_cat(out, track_nodes[node_idx].name);
			str_cat(out, "\r\n");
			// debug_prints(out);
			// debug_printi("time_to_loc_min: ", time_to_loc_min, "\r\n");
			// debug_printi("time_to_loc_max: ", time_to_loc_max, "\r\n");
			// debug_printi("blocked lower bound: ", blocked[node_idx][i][0], "\r\n");
			// debug_printi("blocked upper bound: ", blocked[node_idx][i][1], "\r\n");
			// debug_printi("dist: ", dist, "\r\n");
			// debug_printi("start_time: ", start_time, "\r\n");
			return 1;
		}
	}
	return 0;
}

char is_our_own_parking_spot(uint8_t node, uint8_t* our_parking_spots, uint8_t num_our_parking_spots) {
	for (uint8_t i = 0; i < num_our_parking_spots; i++) {
		if (node == our_parking_spots[i]) {
			return 1;
		}
	}
	return 0;
}

int is_section_blocked2(uint16_t velocity,
						uint32_t blocked[144][6][2],
						int node_idx,
						uint32_t dist,
						uint32_t start_time,
						int parking_spot) {
	debug_prints("is_section_blocked2\r\n");
	uint8_t our_parking_spots[40];
	uint8_t num_our_parking_spots = get_nodes_for_parking(parking_spot, our_parking_spots);
	uint32_t time_to_loc = ((dist * 100) / velocity) + start_time;
	uint32_t time_to_loc_min = time_to_loc < 100 ? 0 : time_to_loc - 100; // - 1 second for error
	uint32_t time_to_loc_max = time_to_loc + 400;						  // + 4 second for error
	if (is_our_own_parking_spot(node_idx, our_parking_spots, num_our_parking_spots)) {
		return 0;
	}

	for (int i = 0; i < 6; i++) {
		if (time_to_loc_min <= blocked[node_idx][i][1] && blocked[node_idx][i][0] <= time_to_loc_max) {
			return 1;
		}
	}
	debug_prints("is_section_blocked2 done\r\n");
	return 0;
}

// checks if we cant park at the given node
// parking spot is our own parking spot, we can park on nodes that are blocked by it
char parking_spot_is_blocked(uint8_t node, uint32_t blocked[144][6][2], uint8_t parking_spot, uint32_t curtime) {
	uint8_t our_parking_spots[40];
	uint8_t num_our_parking_spots = get_nodes_for_parking(parking_spot, our_parking_spots);
	uint8_t parking_spot_nodes[40];
	uint8_t num_parking_spots = get_nodes_for_parking(node, parking_spot_nodes);
	for (uint8_t i = 0; i < num_parking_spots; i++) {
		uint8_t node = parking_spot_nodes[i];
		for (uint8_t j = 0; j < 6; j++) {
			if (blocked[node][j][1] >= curtime
				&& !(blocked[node][j][1] == PARKING_SPOT
					 && is_our_own_parking_spot(node, our_parking_spots, num_our_parking_spots))) {
				return 1;
			}
		}
	}
	return 0;
}

uint8_t first_reverse_on_path_or_dest(uint8_t dest,
									  struct track_path* paths,
									  uint32_t blocked[144][6][2],
									  uint8_t parking_spot,
									  uint32_t curtime,
									  uint8_t* reverse_train) {
	ASSERT(*reverse_train == 0);
	// only give reverse node if we can stop there, otherwise give first sensor
	struct track_path* path_ptr = &paths[dest];
	uint8_t cur_node = 0;
	uint8_t prev_node = 0;
	uint8_t ret_node = dest;
	uint8_t select_first_sensor = 0;
	while (path_ptr != NULL) {
		cur_node = path_ptr->cur->idx;
		if (path_ptr->prev != NULL) {
			prev_node = path_ptr->prev->cur->idx;
		}
		if (track_nodes[cur_node].type == NODE_SENSOR && prev_node == reverse_node(cur_node)) {
			if (!parking_spot_is_blocked(prev_node, blocked, parking_spot, curtime)) {
				ret_node = prev_node;
				*reverse_train = 1;
				select_first_sensor = 0;
			} else {
				select_first_sensor = 1;
				*reverse_train = 0;
			}
		}

		if ((select_first_sensor == 1) && path_ptr->prev == NULL) {
			*reverse_train = 0;
			ret_node = cur_node;
		}
		path_ptr = path_ptr->prev;
	}
	return ret_node;
}

// look back from destination to find the node to travel to
// 1. if the destination is reachable right now, follow path computed by dijkstras tc2 (above fn)
// 2. otherwise, travel to closest node along shortest path
// uint8_t determine_stopping_node(uint8_t dest,
// 								struct track_path* paths,
// 								struct Ring_Buffer* path,
// 								uint32_t blocked[144][6][2],
// 								uint8_t parking_spot,
// 								uint32_t curtime,
// 								uint8_t* reverse_train) {
// 	ASSERT(*reverse_train == 0);
// 	if (paths[dest].dist != UINT16_MAX) {
// 		// if unreachable destination, go on shortest path
// 		return first_reverse_on_path_or_dest(dest, paths, blocked, parking_spot, curtime, reverse_train);
// 	}

// 	uint8_t node = 0xFF;
// 	int i;
// 	for (i = 0; i < path->size; i++) {
// 		node = path->cbuffer[path->r - 1 - i];
// 		if (track_nodes[node].type == NODE_SENSOR && paths[node].dist != UINT16_MAX
// 			&& (!parking_spot_is_blocked(node, blocked, parking_spot, curtime))) {
// 			break;
// 		}
// 	}

// 	// return first reverse node if there is a reverse
// 	uint8_t potential_reverse_node = 0;
// 	while (i < path->size) {
// 		if (i == 0) {
// 			i++;
// 			continue;
// 		}
// 		potential_reverse_node = path->cbuffer[path->r - 1 - i];
// 		// just assume path goes at the start of buffer
// 		if (potential_reverse_node == reverse_node(path->cbuffer[path->r - 1 - i + 1])
// 			&& track_nodes[potential_reverse_node].type == NODE_SENSOR
// 			&& paths[potential_reverse_node].dist != UINT16_MAX
// 			&& (!parking_spot_is_blocked(potential_reverse_node, blocked, parking_spot, curtime))) {
// 			node = potential_reverse_node;
// 			*reverse_train = 1;
// 		}
// 		i++;
// 	}

// 	ASSERT(node != 0xFF);
// 	return node;
// }

// look back from destination to find the node to travel to
// 1. if the destination is reachable right now, follow path computed by dijkstras tc2 (above fn)
// 2. otherwise, travel to closest node along shortest path
uint8_t determine_stopping_node(uint8_t dest,
								struct track_path* paths,
								struct Ring_Buffer* path,
								uint32_t blocked[144][6][2],
								uint8_t parking_spot,
								uint32_t curtime,
								uint8_t* reverse_train) {
	ASSERT(*reverse_train == 0);
	if (paths[dest].dist != UINT16_MAX) {
		// if reachable, go to first reverse or final destination
		return first_reverse_on_path_or_dest(dest, paths, blocked, parking_spot, curtime, reverse_train);
	}

	// cannot reach final node, go down shortest path until we reach first node
	uint8_t ret_node = 0;
	uint8_t potential_reverse_node;
	uint8_t node = path->cbuffer[path->l];
	for (int i = 0; i < path->size; i++) {
		node = path->cbuffer[i];
		if (paths[node].dist == UINT16_MAX) {
			return ret_node;
		}
		if (track_nodes[node].type == NODE_SENSOR && (!parking_spot_is_blocked(node, blocked, parking_spot, curtime))) {
			ret_node = node;
		}
		if (i + 1 < path->size) {
			potential_reverse_node = path->cbuffer[i + 1];
			if (track_nodes[potential_reverse_node].type == NODE_SENSOR && potential_reverse_node == reverse_node(node)
				&& !parking_spot_is_blocked(node, blocked, parking_spot, curtime)) {
				return node;
			}
		}
	}
	return ret_node;
}

char is_parking_spot(uint8_t node, uint8_t parking_spots[20], uint8_t num_parking_spots) {
	for (int i = 0; i < num_parking_spots; i++) {
		if (parking_spots[i] == node) {
			return 1;
		}
	}

	return 0;
}

// implementation where we request full blocked 3d array from track server
uint8_t dijkstra_tc2(uint8_t src,
					 uint8_t dest,
					 uint16_t velocity,
					 uint32_t curtime,
					 uint32_t blocked[144][6][2],
					 uint8_t parking_spot,
					 struct Ring_Buffer* path_buf,
					 uint8_t* reverse_train) {
	struct Ring_Buffer path_buf_internal = create_buffer(CHAR);
	dijkstra(src, dest, &path_buf_internal);
	// print_path("shortest path: ", &path_buf_internal);
	if (path_buf_internal.cbuffer[path_buf_internal.r - 1] != dest) {
		debug_prints("PATH IS NOT REACHABLE\r\n");
		return 0xFF;
	}

	uint8_t parking_spot_nodes[40];
	uint8_t num_parking_spots = get_nodes_for_parking(parking_spot, parking_spot_nodes);

	struct track_path paths[TRACK_NODE_COUNT];
	uint8_t sptSet[TRACK_NODE_COUNT];

	// initialize starting values
	for (int i = 0; i < TRACK_NODE_COUNT; i++) {
		paths[i].dist = UINT16_MAX;
		paths[i].cur = &track_nodes[i];
		paths[i].prev = NULL;
		sptSet[i] = 0;
	}

	paths[src].dist = 0;

	uint8_t next_S, next_C, next_R;
	uint16_t edge_len_S, edge_len_C, edge_len_R;
	for (int count = 0; count < TRACK_NODE_COUNT - 1; count++) {
		uint8_t u = minDistance(paths, sptSet);
		if (paths[u].dist == UINT16_MAX) {
			break;
		}
		struct track_node cur_node = track_nodes[u];
		sptSet[u] = 1;

		switch (cur_node.type) {
		case NODE_SENSOR:
			next_S = cur_node.edge[DIR_AHEAD].dest->idx;
			edge_len_S = cur_node.edge[DIR_AHEAD].dist;
			if (!sptSet[next_S] && paths[u].dist + edge_len_S < paths[next_S].dist
				&& (!is_section_blocked(velocity, blocked, next_S, (paths[u].dist + edge_len_S), curtime)
					|| is_parking_spot(next_S, parking_spot_nodes, num_parking_spots))) {
				paths[next_S].dist = paths[u].dist + edge_len_S;
				paths[next_S].prev = &paths[u];
			}
			// reverse
			next_R = reverse_node(u);
			edge_len_R = (velocity * 5); // count the reverse as equivalent to 5 seconds of time
			if (!sptSet[next_R] && paths[u].dist + edge_len_R < paths[next_R].dist
				&& (!is_section_blocked(velocity, blocked, next_R, (paths[u].dist + edge_len_R), curtime)
					|| is_parking_spot(next_R, parking_spot_nodes, num_parking_spots))) {
				paths[next_R].dist = paths[u].dist + edge_len_R;
				paths[next_R].prev = &paths[u];
			}
			break;
		case NODE_MERGE:
		case NODE_ENTER:
		case NODE_NONE:
			next_S = cur_node.edge[DIR_AHEAD].dest->idx;
			edge_len_S = cur_node.edge[DIR_AHEAD].dist;
			if (!sptSet[next_S] && paths[u].dist + edge_len_S < paths[next_S].dist
				&& (!is_section_blocked(velocity, blocked, next_S, (paths[u].dist + edge_len_S), curtime)
					|| is_parking_spot(next_S, parking_spot_nodes, num_parking_spots))) {
				paths[next_S].dist = paths[u].dist + edge_len_S;
				paths[next_S].prev = &paths[u];
			}
			break;
		case NODE_BRANCH:
			next_S = cur_node.edge[DIR_STRAIGHT].dest->idx;
			next_C = cur_node.edge[DIR_CURVED].dest->idx;
			edge_len_S = cur_node.edge[DIR_STRAIGHT].dist;
			edge_len_C = cur_node.edge[DIR_CURVED].dist;
			if (!sptSet[next_S] && paths[u].dist + edge_len_S < paths[next_S].dist
				&& (!is_section_blocked(velocity, blocked, next_S, (paths[u].dist + edge_len_S), curtime)
					|| is_parking_spot(next_S, parking_spot_nodes, num_parking_spots))) {
				paths[next_S].dist = paths[u].dist + edge_len_S;
				paths[next_S].prev = &paths[u];
			}
			if (!sptSet[next_C] && paths[u].dist + edge_len_C < paths[next_C].dist
				&& (!is_section_blocked(velocity, blocked, next_C, (paths[u].dist + edge_len_C), curtime)
					|| is_parking_spot(next_C, parking_spot_nodes, num_parking_spots))) {
				paths[next_C].dist = paths[u].dist + edge_len_C;
				paths[next_C].prev = &paths[u];
			}
			break;
		case NODE_EXIT:
			break;
		default:
			debug_printi("\r\n", cur_node.type, "\r\n");
			ASSERT(0);
			break;
		}
	}

	struct track_path path = paths[determine_stopping_node(
		dest, paths, &path_buf_internal, blocked, parking_spot, curtime, reverse_train)];
	uint8_t potentially_new_dest = path.cur->idx;

	uint8_t tmp[50];
	uint8_t count = 0;
	while (path.prev) {
		tmp[count] = path.cur->idx;
		count++;
		path = *path.prev;
	}
	// add first node
	tmp[count] = path.cur->idx;
	count++;

	if (*reverse_train == 1) {
		if (count == 1) {
			debug_prints("dijkstra_tc2 chose to reverse train in place\r\n");
			ring_buffer_append(path_buf, tmp[0]);
			return potentially_new_dest;
		} else {
			debug_prints("dijkstra_tc2 chose to reverse train\r\n");
		}
	}

	// if (potentially_new_dest != dest) {
	// 	char debug_msg_prefix[] = "Not going to final dest, instead going to ";
	// 	char debug_msg[charsize(debug_msg_prefix) + 2 + 5];
	// 	str_copy(debug_msg_prefix, debug_msg);
	// 	str_cat(debug_msg, track_nodes[tmp[0]].name);
	// 	str_cat(debug_msg, "\r\n");
	// 	debug_prints(debug_msg);
	// }

	if (count <= 1) {
		// don't reserve nodes if no path, this check may need to be updated
		return potentially_new_dest;
	}

	// on a path A->B->C->D
	// want to reserve A for time from getting to A to time getting to B
	// want to reserve B for time getting from B to C, add buffer time
	uint32_t time_to_next_node = 0;
	uint32_t time_to_cur_node = 0;
	uint32_t distance_to_prev_node = 0;
	int next_node = 0;
	int track_admin_tid = get_tid_by_name(TRACK_ADMIN);
	for (int i = 0; i < count - 1; i++) {
		next_node = tmp[count - i - 2];
		time_to_next_node = time_to_cur_node + ((paths[next_node].dist - distance_to_prev_node) * 100) / velocity;
		reserve_nodes(tmp + (count - i - 1),
					  1,
					  track_admin_tid,
					  curtime + time_to_cur_node,
					  curtime + time_to_next_node + 1); // +1 as some nodes have 0 distance to node ahead
		distance_to_prev_node = paths[next_node].dist;
		time_to_cur_node = time_to_next_node;
	}

	for (int i = 0; i < count; i++) {
		ring_buffer_append(path_buf, tmp[count - i - 1]);
	}

	print_path("dijkstra_tc2 chose path: ", path_buf);

	return potentially_new_dest;
}

int distance_to_next_sensor_on_path(struct Ring_Buffer* path_buf, struct track_node* cur_loc) {
	uint16_t dist = 0;

	for (int i = 0; i < path_buf->size - 1; i++) {
		uint8_t node_idx = path_buf->cbuffer[(path_buf->l + i) % g_max_cbuffer_elements];
		struct track_node node = track_nodes[node_idx];

		if (node.type == NODE_BRANCH) {
			if (node.edge[DIR_STRAIGHT].dest->idx
				== path_buf->cbuffer[(path_buf->l + i + 1) % g_max_cbuffer_elements]) {
				dist += node.edge[DIR_STRAIGHT].dist;
			} else if (node.edge[DIR_CURVED].dest->idx
					   == path_buf->cbuffer[(path_buf->l + i + 1) % g_max_cbuffer_elements]) {
				dist += node.edge[DIR_CURVED].dist;
			} else {
				ASSERT(0);
			}
		} else if (node.type == NODE_SENSOR) {
			break;
		} else {
			dist += node.edge[DIR_AHEAD].dist;
		}
	}

	// debug_printi("Distance to dest: ", dist, " \r\n");

	return dist + cur_loc->edge[DIR_AHEAD].dist;
}

int sensors_left(struct Ring_Buffer* path_buf) {
	uint16_t sensors_left = 0;

	for (int i = 0; i < path_buf->size; i++) {
		uint8_t node_idx = path_buf->cbuffer[(path_buf->l + i) % g_max_cbuffer_elements];
		struct track_node node = track_nodes[node_idx];

		if (node.type == NODE_SENSOR) {
			sensors_left++;
		}
	}

	// debug_printi("Distance to dest: ", dist, " \r\n");

	return sensors_left;
}

// returns next branch on path, 0xFF if not possible
uint8_t next_branch_from_sensor(uint8_t node) {
	uint8_t next_node_idx = node;
	for (int i = 0; i < 10; i++) {
		uint8_t node_idx = next_node_idx;
		struct track_node node = track_nodes[node_idx];

		if (node.type == NODE_BRANCH) {
			return node_idx;
		} else {
			next_node_idx = node.edge[DIR_AHEAD].dest->idx;
		}
	}
	return 0xFF;
}
