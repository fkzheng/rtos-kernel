#include "request.h"

struct Syscall_Request
create_request(enum syscall request_id, uint64_t arg1, uint64_t arg2, uint64_t arg3, uint64_t arg4, uint64_t arg5) {
	struct Syscall_Request request
		= { .request_id = (uint64_t)request_id, .arg1 = arg1, .arg2 = arg2, .arg3 = arg3, .arg4 = arg4, .arg5 = arg5 };

	return request;
}

/******** Request for Name Server *********/

struct Name_Server_Request create_name_server_request(const char name_server_request, const char* data, const int tid) {
	struct Name_Server_Request r = {
		.request_type = name_server_request,
	};
	switch (name_server_request) {
	case NAME_SERVER_REGISTER:
		str_copy(data, r.name);
		r.tid = tid;
		break;
	case NAME_SERVER_UNREGISTER:
		r.tid = tid;
		break;
	case NAME_SERVER_WHOIS:
		str_copy(data, r.name);
		break;
	}
	return r;
}

/******** Request for RPS Server *********/

struct RPS_Request create_rps_request(uint8_t request_id, uint8_t weapon) {
	struct RPS_Request request = { .request_id = request_id, .weapon = weapon };

	return request;
}

/******** Request for Clock Server *********/

struct Clock_Server_Request create_clock_request(enum clock_server_request request_type, uint32_t arg1) {
	struct Clock_Server_Request request = { .request_type = request_type, .arg1 = arg1 };

	return request;
}

/******** Request for UART Server *********/

struct UART_Server_Request create_uart_request(enum uart_server_request request_type, const char* msg) {
	struct UART_Server_Request request = { .request_type = request_type };
	str_copy(msg, request.msg);
	return request;
}