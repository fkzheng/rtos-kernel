#include "random.h"

static const char RANDOM_SERVER[] = "RANDOM_SERVER";

struct random_request {
	char _unused;
};
void random_server() {
	RegisterAs(RANDOM_SERVER);
	// A: 69
	// B: 54
	uint64_t SEED = 69;
	uint64_t MODULO = (SENSOR_COUNT * SENSOR_MAX_NUMBER);
	uint64_t MULTIPLIER = 1103515245;
	uint64_t INCREMENT = 12345;
	uint64_t DIVISOR = 65536;

	// for msg passing
	int sender;
	char rcvbuf[sizeof(struct random_request)];
	char reply = SEED % MODULO;
	uint64_t next = SEED;

	for (;;) {
		Receive(&sender, rcvbuf, sizeof(struct random_request));
		Reply(sender, (char*)&reply, 1);
		next = ((next * MULTIPLIER) + INCREMENT) / DIVISOR;
		reply = next % MODULO;
		// A13, A14, B7, B8, B9, B10, B11, B12, C3, C4, B3, B4, (FOR TRACK A: A11, A12, A15, A16)
		while (reply == 12 || reply == 13 || reply == 22 || reply == 23 || reply == 24 || reply == 25 || reply == 26
			   || reply == 27 || reply == 34 || reply == 35 || reply == 18 || reply == 19 || reply == 10 || reply == 11
			   || reply == 14 || reply == 15) {
			next = ((next * MULTIPLIER) + INCREMENT) / DIVISOR;
			reply = next % MODULO;
		}
	}
}

uint8_t get_random_sensor() {
	int rng_tid = get_tid_by_name(RANDOM_SERVER);
	struct random_request req = { ._unused = 0 };
	char replybuf[1];
	Send(rng_tid, (char*)&req, sizeof(struct random_request), replybuf, 1);
	ASSERT(replybuf[0] < 80);
	return (uint8_t)replybuf[0];
}