#include "train_control.h"

#define FORWARDS 0
#define BACKWARDS 1

const char TRAIN_STOP = 0;
const char TRAIN_MAX_SPEED = 14;
const char TRAIN_REVERSE = 15;
const char TRAIN_LIGHTS_ON = 16;
const uint16_t BACK_UP_TIME = 300;
// trains { 1, 2, 24, 58, 74, 78 };

const char g_train_speeds[6][3]
	= { { 2, 7, 14 }, { 2, 5, 14 }, { 5, 8, 14 }, { 5, 9, 14 }, { 2, 5, 14 }, { 6, 8, 14 } };

struct Train_Info {
	uint8_t train_num;
	uint8_t train_speed;
	uint16_t stopping_distance; // from medium speed, in μm
	uint32_t train_velocity[3];
	uint16_t train_acceleration[3]; // needs change probably, unused currently
	uint32_t last_sensor_trigger_time;
	uint32_t predicted_next_sensor_time; // absolute time, not needed
	uint32_t predicted_time_to_sensor;	 // time to the sensor, used for error calculations
	uint32_t alpha_denom;
	struct track_node* prev_loc;
	struct track_node* cur_loc;
	struct Ring_Buffer path;
	char has_path;
	char stopping;
	uint8_t parking_spot;
	uint8_t cur_dest;
	uint8_t reversing;
	uint64_t offset;
	// EWMA
	uint64_t next_sensor_distance;

	// TC2 Sensor Attribution all the time
	int current_sensor;
	int possible_next_sensors[8];

	// UI stuff with path
	int time_error;
	int dist_error;

	// 0: forward, 1: reverse
	char direction;
};

static void send_train_command(int tid, const char* cmd) {
	// we don't need the reply
	char reply[1];
	struct UART_Server_Request req = create_uart_request(UART_SEND_TRAIN, cmd);
	Send(tid, (char*)&req, sizeof(struct UART_Server_Request), reply, 1);
}

static void honk() {
	// we don't need the reply
	char honk_78[3] = { 79, 78, 0 };
	char honk_58[3] = { 79, 58, 0 };
	char reply[1];
	struct UART_Server_Request req1 = create_uart_request(UART_SEND_TRAIN, honk_78);
	struct UART_Server_Request req2 = create_uart_request(UART_SEND_TRAIN, honk_58);
	Send(get_tid_by_name(UART_SERVER_TRAIN_SEND), (char*)&req1, sizeof(struct UART_Server_Request), reply, 1);
	Send(get_tid_by_name(UART_SERVER_TRAIN_SEND), (char*)&req2, sizeof(struct UART_Server_Request), reply, 1);
}

void send_speed_command(char train_num, char speed) {
	char reply_buf[1];
	struct Train_Worker_Request train_worker_request;
	char train_worker_name[] = "TRAIN_WORKER_XX";
	if (train_num != 1 && train_num != 2 && train_num != 24 && train_num != 58 && train_num != 74 && train_num != 78) {
		debug_prints("Invalid Train Number\r\n");
		return;
	}
	get_train_worker_name(train_num, train_worker_name);
	int worker_id = get_tid_by_name(train_worker_name);
	train_worker_request.request_type = TR;
	train_worker_request.speed = speed;
	Send(worker_id, (char*)&train_worker_request, sizeof(struct Train_Worker_Request), reply_buf, 1);
}

void send_reverse_command(char train_num) {
	char reply_buf[1];
	struct Train_Worker_Request train_worker_request;
	char train_worker_name[] = "TRAIN_WORKER_XX";
	if (train_num != 1 && train_num != 2 && train_num != 24 && train_num != 58 && train_num != 74 && train_num != 78) {
		prints("\r\nInvalid Train Number");
		return;
	}
	get_train_worker_name(train_num, train_worker_name);
	int worker_id = get_tid_by_name(train_worker_name);
	train_worker_request.request_type = RV;
	Send(worker_id, (char*)&train_worker_request, sizeof(struct Train_Worker_Request), reply_buf, 1);
}

struct reverse_instantly_request {
	uint8_t train_num;
};

void _reverse_instantly(char train_num) {
	char reply_buf[1];
	struct Train_Worker_Request train_worker_request;
	char train_worker_name[] = "TRAIN_WORKER_XX";
	if (train_num != 1 && train_num != 2 && train_num != 24 && train_num != 58 && train_num != 74 && train_num != 78) {
		prints("\r\nInvalid Train Number");
		return;
	}
	get_train_worker_name(train_num, train_worker_name);
	int worker_id = get_tid_by_name(train_worker_name);
	train_worker_request.request_type = RV_I;
	Send(worker_id, (char*)&train_worker_request, sizeof(struct Train_Worker_Request), reply_buf, 1);
}

void reverse_instantly_worker() {
	char rcvbuf[sizeof(struct reverse_instantly_request)];
	int sender_tid;
	struct reverse_instantly_request* req = (struct reverse_instantly_request*)rcvbuf;
	Receive(&sender_tid, rcvbuf, sizeof(struct reverse_instantly_request));
	Reply(sender_tid, rcvbuf, 1);
	_reverse_instantly(req->train_num);
	Exit();
}

void reverse_instantly(char train_num) {
	struct reverse_instantly_request req = { .train_num = train_num };
	int child_tid = Create(2, reverse_instantly_worker);
	char reply;
	Send(child_tid, (char*)&req, sizeof(struct reverse_instantly_request), &reply, 1);
}

void set_path2(uint8_t train, uint8_t dest, uint64_t offset) {
	int train_admin_tid = get_tid_by_name(TRAIN_ADMIN);
	char replybuf[1];
	struct Train_Admin_Request req
		= { .request_type = TA_PATH_UPDATE, .train_num = train, .dest = dest, .offset = offset };
	Send(train_admin_tid, (char*)&req, sizeof(struct Train_Admin_Request), replybuf, 1);
}

void toggle_random_paths() {
	int train_admin_tid = get_tid_by_name(TRAIN_ADMIN);
	char replybuf[1];
	struct Train_Admin_Request req = { .request_type = TA_RNG };
	Send(train_admin_tid, (char*)&req, sizeof(struct Train_Admin_Request), replybuf, 1);
}

void loop_stop(uint8_t dest) {
	int train_admin_tid = get_tid_by_name(TRAIN_ADMIN);
	char reply;
	struct Train_Admin_Request req = { .request_type = TA_LOOP_STOP, .dest = dest };
	Send(train_admin_tid, (char*)&req, sizeof(struct Train_Admin_Request), &reply, 1);
}

void set_cur_train(char train) {
	char reply;
	struct Train_Admin_Request req = { .request_type = TA_SET_TRAIN, .train_num = train, .update = 0 };
	Send(get_tid_by_name(TRAIN_ADMIN), (char*)&req, sizeof(struct Train_Admin_Request), &reply, 1);
}

int train_num_to_index(char train_num) {
	char legal_trains[] = { 1, 2, 24, 58, 74, 78 };
	int num_trains = 6;
	for (int i = 0; i < num_trains; i++) {
		if (legal_trains[i] == train_num) {
			return i;
		}
	}
	debug_printi("Got unexpected train_num to translate: ", train_num, "\r\n");
	ASSERT(0);
	return 0;
}

void locate_train(int train_num, int sensor) {
	char reply;
	struct Train_Admin_Request req = { .request_type = TA_LOCATE_TRAIN, .train_num = train_num, .update = sensor };
	Send(get_tid_by_name(TRAIN_ADMIN), (char*)&req, sizeof(struct Train_Admin_Request), &reply, 1);
}

void train_worker() {
	// worker expects a 1 character message where msg[0] is the train number it is responsible for
	char msg[1];
	int parent = 0;
	Receive(&parent, msg, 1);
	Reply(parent, msg, 1);

	int train_num = msg[0];
	char task_name[] = "TRAIN_WORKER_XX";
	get_train_worker_name(train_num, task_name);
	RegisterAs(task_name);

	int cur_speed = 0;

	int uart_train_tx_tid = get_tid_by_name(UART_SERVER_TRAIN_SEND);
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	char buf[sizeof(struct Train_Worker_Request)];
	char reply[] = "R";
	int sender = 0;

	int col = (train_num_to_index(train_num) * 5) + 40;
	char debug_msg1[] = "\0337\033[2;00H\033[32mTW00\033[0m\0338";
	char debug_msg2[] = "\0337\033[2;00H\033[31mTW00\033[0m\0338";
	debug_msg1[7] = col % 10 + '0';
	debug_msg1[6] = col / 10 + '0';
	debug_msg2[7] = col % 10 + '0';
	debug_msg2[6] = col / 10 + '0';

	debug_msg1[16] = train_num / 10 + '0';
	debug_msg1[17] = train_num % 10 + '0';
	debug_msg2[16] = train_num / 10 + '0';
	debug_msg2[17] = train_num % 10 + '0';
	for (;;) {
		prints(debug_msg1);
		Receive(&sender, buf, sizeof(struct Train_Worker_Request));
		prints(debug_msg2);
		Reply(sender, reply, 1);
		struct Train_Worker_Request* request = (struct Train_Worker_Request*)buf;
		if (request->request_type == TR) {
			if (!(request->speed >= TRAIN_STOP && request->speed <= TRAIN_MAX_SPEED)) {
				debug_prints("invalid speed\r\n");
				continue;
			} else if (cur_speed == request->speed) {
				continue;
			}

			char cmd[] = { request->speed + TRAIN_LIGHTS_ON, train_num, '\0' };
			send_train_command(uart_train_tx_tid, cmd);
			cur_speed = request->speed;

			int train_admin_tid = get_tid_by_name(TRAIN_ADMIN);
			char replybuf[1];
			struct Train_Admin_Request req
				= { .request_type = TA_SPEED_UPDATE, .train_num = train_num, .update = request->speed };
			Send(train_admin_tid, (char*)&req, sizeof(struct Train_Admin_Request), replybuf, 1);
		} else if (request->request_type == RV) {
			char stop_cmd[] = { TRAIN_STOP + TRAIN_LIGHTS_ON, train_num, '\0' };
			send_train_command(uart_train_tx_tid, stop_cmd);
			char reverse[] = { TRAIN_REVERSE, train_num, '\0' };
			Delay(clock_server_tid, 500);
			send_train_command(uart_train_tx_tid, reverse);

			char set_speed[] = { cur_speed + TRAIN_LIGHTS_ON, train_num, '\0' };
			Delay(clock_server_tid, 10);
			send_train_command(uart_train_tx_tid, set_speed);
		} else if (request->request_type == RV_I) {
			// reverse immediately and turn on the lights
			char reverse[] = { TRAIN_REVERSE, train_num, '\0' };
			send_train_command(uart_train_tx_tid, reverse);
			Delay(clock_server_tid, 10);
			char stop_cmd[] = { TRAIN_STOP + TRAIN_LIGHTS_ON, train_num, '\0' };
			send_train_command(uart_train_tx_tid, stop_cmd);
		} else {
			debug_printi("INVALID REQUEST: ", request->request_type, "\r\n");
			ASSERT(0);
		}
	}
}

int is_legal_train_num(uint8_t train_num) {
	char legal_trains[] = { 1, 2, 24, 58, 74, 78 };
	int found = 0;
	for (int i = 0; i < 6; i++) {
		if (train_num == legal_trains[i]) {
			found = 1;
		}
	}
	return found;
}

uint64_t distance_to_dest(struct Ring_Buffer* path, struct track_node* curnode) {
	if (path->size == 0) {
		return 0;
	}
	uint64_t dist = 0;

	for (int i = 0; i < path->size - 1; i++) {
		uint8_t node_idx = path->cbuffer[(path->l + i) % g_max_cbuffer_elements];
		struct track_node node = track_nodes[node_idx];

		if (node.type == NODE_BRANCH) {
			if (node.edge[DIR_STRAIGHT].dest->idx == path->cbuffer[(path->l + i + 1) % g_max_cbuffer_elements]) {
				dist += node.edge[DIR_STRAIGHT].dist;
			} else if (node.edge[DIR_CURVED].dest->idx == path->cbuffer[(path->l + i + 1) % g_max_cbuffer_elements]) {
				dist += node.edge[DIR_CURVED].dist;
			} else {
				ASSERT(0);
			}
		} else {
			dist += node.edge[DIR_AHEAD].dist;
		}
	}

	// debug_printi("Distance to dest: ", dist, " \r\n");

	return dist + curnode->edge[DIR_AHEAD].dist;
}

void update_path(struct Ring_Buffer* path, struct track_node* cur_node, char train_num) {
	uint8_t node_idx = 1;
	struct track_node path_node;
	path_node.idx = -1;
	while (!ring_buffer_is_empty(path)) {
		node_idx = ring_buffer_pop(path);
		path_node = track_nodes[node_idx];
		// prints("| ");
		// prints(path_node.name);
		// prints(" ");

		if (cur_node->idx == path_node.idx) {
			break;
		}
	}

	if (cur_node->idx != path_node.idx) {
		// TODO: maybe stop and repath here
		debug_prints("WE ARE NOT FOLLOWING THE PATH STOPPING\r\n");
		send_speed_command(train_num, 0);
	}
}

void init_train_metadata(struct Train_Info* train_metadata) {
	train_metadata[0].train_num = 1;
	train_metadata[1].train_num = 2;
	train_metadata[2].train_num = 24;
	train_metadata[3].train_num = 58;
	train_metadata[4].train_num = 74;
	train_metadata[5].train_num = 78;

	train_metadata[0].parking_spot = 0;
	train_metadata[1].parking_spot = 0;
	train_metadata[2].parking_spot = 0;
	train_metadata[3].parking_spot = 0;
	train_metadata[4].parking_spot = 0;
	train_metadata[5].parking_spot = 0;

	// train 24 - place on A1
	train_metadata[2].train_speed = 0;
	train_metadata[2].train_velocity[0] = 75; // mm/s
	train_metadata[2].train_velocity[1] = 217;
	train_metadata[2].train_velocity[2] = 564;
	train_metadata[2].stopping_distance = 177;
	train_metadata[2].alpha_denom = 2;
	train_metadata[2].has_path = 0;
	train_metadata[2].cur_loc = &track_nodes[0];
	train_metadata[2].cur_dest = 0;
	train_metadata[2].parking_spot = 0;
	train_metadata[2].possible_next_sensors[0] = 44;
	train_metadata[2].possible_next_sensors[1] = -1;
	train_metadata[2].possible_next_sensors[2] = -1;
	train_metadata[2].possible_next_sensors[3] = -1;
	train_metadata[2].time_error = 0;
	train_metadata[2].dist_error = 0;
	train_metadata[2].reversing = 0;
	// park_train(0, get_tid_by_name(TRACK_ADMIN));

	// train 58 - place on A12
	train_metadata[3].train_speed = 0;
	train_metadata[3].train_velocity[0] = 67; // mm/s
	train_metadata[3].train_velocity[1] = 249;
	train_metadata[3].train_velocity[2] = 544;
	train_metadata[3].stopping_distance = 228;
	train_metadata[3].alpha_denom = 2;
	train_metadata[3].has_path = 0;
	train_metadata[3].cur_loc = &track_nodes[12];
	train_metadata[3].cur_dest = 12;
	train_metadata[3].parking_spot = 12;
	train_metadata[3].possible_next_sensors[0] = 44;
	train_metadata[3].possible_next_sensors[1] = -1;
	train_metadata[3].possible_next_sensors[2] = -1;
	train_metadata[3].possible_next_sensors[3] = -1;
	train_metadata[3].time_error = 0;
	train_metadata[3].dist_error = 0;
	train_metadata[3].reversing = 0;
	// park_train(12, get_tid_by_name(TRACK_ADMIN));

	// train 74 - place on A1
	train_metadata[4].train_speed = 0;
	train_metadata[4].train_velocity[0] = 77; // mm/s
	train_metadata[4].train_velocity[1] = 222;
	train_metadata[4].train_velocity[2] = 425;
	train_metadata[4].stopping_distance = 190;
	train_metadata[4].alpha_denom = 2;
	train_metadata[4].has_path = 0;
	train_metadata[4].cur_loc = &track_nodes[4];
	train_metadata[4].cur_dest = 4;
	train_metadata[4].parking_spot = 4;
	train_metadata[4].possible_next_sensors[0] = 38;
	train_metadata[4].possible_next_sensors[1] = -1;
	train_metadata[4].possible_next_sensors[2] = -1;
	train_metadata[4].possible_next_sensors[3] = -1;
	train_metadata[4].time_error = 0;
	train_metadata[4].dist_error = 0;
	train_metadata[4].reversing = 0;

	// train 78 - place on A12
	train_metadata[5].train_speed = 0;
	train_metadata[5].train_velocity[0] = 89; // mm/s
	train_metadata[5].train_velocity[1] = 172;
	train_metadata[5].train_velocity[2] = 564;
	train_metadata[5].stopping_distance = 110;
	train_metadata[5].alpha_denom = 2;
	train_metadata[5].has_path = 0;
	train_metadata[5].cur_loc = &track_nodes[12];
	train_metadata[5].cur_dest = 12;
	train_metadata[5].parking_spot = 12;
	train_metadata[5].possible_next_sensors[0] = 44;
	train_metadata[5].possible_next_sensors[1] = -1;
	train_metadata[5].possible_next_sensors[2] = -1;
	train_metadata[5].possible_next_sensors[3] = -1;
	train_metadata[5].time_error = 0;
	train_metadata[5].dist_error = 0;
	train_metadata[5].reversing = 0;

	// uncalibrated trains
	train_metadata[0].train_speed = 0;
	train_metadata[0].train_velocity[0] = 0; // mm/s
	train_metadata[0].train_velocity[1] = 0;
	train_metadata[0].train_velocity[2] = 0;
	train_metadata[0].stopping_distance = 0;
	train_metadata[0].alpha_denom = 2;
	train_metadata[0].has_path = 0;
	train_metadata[0].cur_loc = &track_nodes[134];
	train_metadata[0].cur_dest = 134;
	train_metadata[0].possible_next_sensors[0] = -1;
	train_metadata[0].possible_next_sensors[1] = -1;
	train_metadata[0].possible_next_sensors[2] = -1;
	train_metadata[0].possible_next_sensors[3] = -1;
	train_metadata[0].time_error = 0;
	train_metadata[0].dist_error = 0;
	train_metadata[1].train_speed = 0;
	train_metadata[1].train_velocity[0] = 0; // mm/s
	train_metadata[1].train_velocity[1] = 0;
	train_metadata[1].train_velocity[2] = 0;
	train_metadata[1].stopping_distance = 0;
	train_metadata[1].alpha_denom = 2;
	train_metadata[1].has_path = 0;
	train_metadata[1].cur_loc = &track_nodes[134];
	train_metadata[1].cur_dest = 134;
	train_metadata[1].possible_next_sensors[0] = -1;
	train_metadata[1].possible_next_sensors[1] = -1;
	train_metadata[1].possible_next_sensors[2] = -1;
	train_metadata[1].possible_next_sensors[3] = -1;
	train_metadata[1].time_error = 0;
	train_metadata[1].dist_error = 0;
	train_metadata[1].reversing = 0;

	for (int i = 0; i < 6; i++) {
		train_metadata[i].direction = 0;
	}
}

// small stopping task to control delay when stopping
struct stop_req {
	int curtime;
	char train_num;
	uint64_t velocity;
	uint64_t distance_to_dest;
	uint64_t stopping_distance;
};

void delay_and_stop() {
	int admin_tid;
	char reply[1] = { 0 };
	char rcvbuf[sizeof(struct stop_req)];
	Receive(&admin_tid, rcvbuf, sizeof(struct stop_req));
	Reply(admin_tid, reply, 1);

	int clock_tid = get_tid_by_name(CLOCK_SERVER);

	struct stop_req* req = (struct stop_req*)rcvbuf;

	// mm / mm/ticks / 100 -> ticks
	uint64_t ticks_to_dest = ((req->distance_to_dest - req->stopping_distance) * 100) / req->velocity;
	// debug_printi("dist 2 dest: ", req->distance_to_dest, "\r\n");
	// debug_printi("stop dist: ", req->stopping_distance, "\r\n");
	// debug_printi("velocity: ", req->velocity, "\r\n");
	// debug_printi("calculated ticks to stop: ", ticks_to_dest, "\r\n");
	DelayUntil(clock_tid, req->curtime + ticks_to_dest);
	send_speed_command(req->train_num, 0);

	Exit();
}

char sensor_to_node(char sensor_letter, int sensor_number) {
	ASSERT(sensor_letter <= 'E' && sensor_letter >= 'A');
	ASSERT(sensor_number <= 16 && sensor_number >= 1);
	return (char)((sensor_letter - 'A') * 16) + sensor_number - 1;
}

char next_sensor_in_loop(char sensor, char* sensor_loop, int loop_length) {
	for (int i = 0; i < loop_length; i++) {
		if (sensor_loop[i] == sensor) {
			return sensor_loop[i + 1];
		}
	}
	ASSERT(0);
	return 0;
}

void handle_possible_stop(struct Train_Info* train_metadata,
						  uint64_t sensors_to_dest,
						  uint64_t dist_to_dest,
						  uint32_t curtime,
						  char cur_train,
						  uint64_t cur_train_ind) {
	char reply[1] = { 0 };
	if (!train_metadata[cur_train_ind].stopping && sensors_to_dest <= 1
		&& (train_metadata[cur_train_ind].train_speed == g_train_speeds[cur_train_ind][0])) {
		// Low speed
		int stop_tid = Create(1, delay_and_stop);

		struct stop_req req = { .curtime = curtime,
								.train_num = cur_train,
								.velocity = train_metadata[cur_train_ind].train_velocity[0],
								.distance_to_dest = dist_to_dest,
								.stopping_distance = 0 };
		Send(stop_tid, (char*)&req, sizeof(struct stop_req), reply, 1);
		train_metadata[cur_train_ind].stopping = 1;
	} else if (!train_metadata[cur_train_ind].stopping && sensors_to_dest <= 1
			   && (train_metadata[cur_train_ind].train_speed == g_train_speeds[cur_train_ind][1])) {
		// medium speed logic:
		int stop_tid = Create(1, delay_and_stop);
		char reply[1];
		struct stop_req req = { .curtime = curtime,
								.train_num = cur_train,
								.velocity = train_metadata[cur_train_ind].train_velocity[1],
								.distance_to_dest = dist_to_dest,
								.stopping_distance = train_metadata[cur_train_ind].stopping_distance };
		Send(stop_tid, (char*)&req, sizeof(struct stop_req), reply, 1);
		train_metadata[cur_train_ind].stopping = 1;
	} else if (!train_metadata[cur_train_ind].stopping
			   && (train_metadata[cur_train_ind].train_speed == g_train_speeds[cur_train_ind][2])
			   && (dist_to_dest < 2500)) {
		// High speed
		send_speed_command(cur_train, g_train_speeds[cur_train_ind][1]);
	}
}

int check_which_train_triggered_sensor(struct Train_Info train_metadata[], int sensor_id) {
	int possible_train = 0;
	uint32_t min_arrival_time = UINT32_MAX;
	// must have path
	for (int i = 0; i < 6; i++) {
		// we only move the trains if they have a path
		if (!train_metadata[i].has_path) {
			continue;
		}
		for (int j = 0; j < 8; j++) {
			if (train_metadata[i].possible_next_sensors[j] == sensor_id) {
				if (train_metadata[i].predicted_next_sensor_time < min_arrival_time) {
					min_arrival_time = train_metadata[i].predicted_next_sensor_time;
					possible_train = train_metadata[i].train_num;
				}
			}
		}
	}

	// if train not found, check train 24 (user controlled)
	if (!possible_train) {
		for (int j = 0; j < 8; j++) {
			if (train_metadata[2].possible_next_sensors[j] == sensor_id) {
				possible_train = train_metadata[2].train_num;
			}
		}
	}

	// if no train expected to go over the sensor, we ignore and return 0
	return possible_train;
}

int update_sensor_attribution(struct Train_Info train_metadata[], int cur_train_ind) {
	// clear the possible next sensors
	for (int i = 0; i < 8; i++) {
		train_metadata[cur_train_ind].possible_next_sensors[i] = -1;
	}
	// bfs for next sensor including branches
	ASSERT(train_metadata[cur_train_ind].cur_loc && train_metadata[cur_train_ind].cur_loc->idx <= 144
		   && train_metadata[cur_train_ind].cur_loc->idx >= 0);
	struct track_node* cur = train_metadata[cur_train_ind].cur_loc;
	int queue[40];
	int front = 0, rear = 0, count = 0;
	queue[rear++] = cur->edge[DIR_AHEAD].dest->idx;
	queue[rear++] = cur->reverse->idx;
	while (front != rear && front < 40) {
		int node_idx = queue[front++];
		struct track_node node = track_nodes[node_idx];

		if (node.type == NODE_SENSOR) {
			train_metadata[cur_train_ind].possible_next_sensors[count++] = track_nodes[node_idx].idx;
		} else if (node.type == NODE_BRANCH) {
			queue[rear++] = node.edge[DIR_STRAIGHT].dest->idx;
			queue[rear++] = node.edge[DIR_CURVED].dest->idx;
		} else if (node.type == NODE_NONE || node.type == NODE_MERGE) {
			queue[rear++] = node.edge[DIR_AHEAD].dest->idx;
		}
	}

	return count;
}

void try_to_start_path() {
	char rcv_buf[sizeof(struct Train_Admin_Request)];
	int sender_tid = 0;
	char reply = 0;
	Receive(&sender_tid, rcv_buf, sizeof(struct Train_Admin_Request));
	Reply(sender_tid, &reply, 1);
	struct Train_Admin_Request* req = (struct Train_Admin_Request*)rcv_buf;
	req->request_type = TA_TRY_TO_START_PATH;
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	int count = 0;
	while (reply == 0) {
		// debug_printi("attempting to set path for train ", req->train_num, "\r\n");
		Send(sender_tid, (char*)req, sizeof(struct Train_Admin_Request), &reply, 1);
		Delay(clock_server_tid, 100);
		count++;
		if (count >= 5) {
			debug_printi("Detected likely deadlock for train: ", req->train_num, ", pathing to random location\r\n");
			req->dest = get_random_sensor();
			while (req->dest == req->src) {
				req->dest = get_random_sensor();
			}
			debug_printi("Random location picked: ", req->dest, "\r\n");
		}
	}
	if (reply == 1) {
		debug_printi("sucessfully set path for train: ", req->train_num, "\r\n");
		debug_printi("New destination: ", req->dest, "\r\n");
	} else {
		debug_printi("path given for train ", req->train_num, " is impossible, please choose another\r\n");
	}
	Exit();
}

void _init_train_ui(int clock_server_tid) {
	char train_ui_base1[]
		= "\0337"
		  "\033[5;40H╔═══════════════════════╦═══════════════════════╦═══════════════════════╦═══════════════════════╦═"
		  "══════════════════════╦═══════════════════════╗"
		  "\033[6;40H║          1            ║          2            ║         24            ║         58            "
		  "║ "
		  "        74            ║         78            ║"
		  "\0338";
	char train_ui_base2[]
		= "\0337"
		  "\033[7;40H╠═══════════════════════╬═══════════════════════╬═══════════════════════╬═══════════════════════╬═"
		  "══════════════════════╬═══════════════════════╣"
		  "\033[8;40H║ Speed: -              ║ Speed: -              ║ Speed: -              ║ Speed: -              ║ "
		  "Speed: -              ║ Speed: -              ║"
		  "\033[9;40H║ Velocity (mm/s): -    ║ Velocity (mm/s): -    ║ Velocity (mm/s): -    ║ Velocity (mm/s): -    ║ "
		  "Velocity (mm/s): -    ║ Velocity (mm/s): -    ║"
		  "\0338";
	char train_ui_base3[] = "\0337"
							"\033[10;40H║ Location: -           ║ Location: -           ║ Location: -           ║ "
							"Location: -           ║ "
							"Location: -           ║ Location: -           ║"
							"\033[11;40H║ Next Sensor: -        ║ Next Sensor: -        ║ Next Sensor: -        ║ "
							"Next Sensor: -        ║ "
							"Next Sensor: -        ║ Next Sensor: -        ║"
							"\0338";
	char train_ui_base4[] = "\0337"
							"\033[12;40H║ Time Δ: -             ║ Time Δ: -             ║ Time Δ: -             ║ "
							"Time Δ: -             ║ "
							"Time Δ: -             ║ Time Δ: -             ║"
							"\033[13;40H║ Distance Δ: -         ║ Distance Δ: -         ║ Distance Δ: -         ║ "
							"Distance Δ: -         ║ "
							"Distance Δ: -         ║ Distance Δ: -         ║"
							"\0338";
	char train_ui_base5[]
		= "\0337"
		  "\033[14;40H║ Destination: -        ║ Destination: -        ║ Destination: -        ║ Destination: -        "
		  "║ "
		  "Destination: -        ║ Destination: -        ║"
		  "\033[15;"
		  "40H╚═══════════════════════╩═══════════════════════╩═══════════════════════╩═══════════════════════╩═"
		  "══════════════════════╩═══════════════════════╝"
		  "\0338";

	prints(train_ui_base1);
	Delay(clock_server_tid, 10);
	prints(train_ui_base2);
	Delay(clock_server_tid, 10);
	prints(train_ui_base3);
	Delay(clock_server_tid, 10);
	prints(train_ui_base4);
	Delay(clock_server_tid, 10);
	prints(train_ui_base5);
}

void _print_ui_int(int row, int col, int value) {
	char out[50];
	char prefix[] = "\0337";
	char suffix[] = "\0338";
	char pos_str[] = "\033[XX;XXXH";
	str_copy(prefix, out);

	pos_str[2] = (row / 10) + '0';
	pos_str[3] = (row % 10) + '0';
	pos_str[5] = (col / 100) + '0';
	pos_str[6] = ((col / 10) % 10) + '0';
	pos_str[7] = (col % 10) + '0';
	str_cat(out, pos_str);
	str_cat(out, "   ");
	str_cat(out, pos_str);
	printi_maybe_negative(out, value, suffix);
}

void _print_ui_sensor(int row, int col, int track_idx) {
	char out[50];
	char prefix[] = "\0337";
	char suffix[] = "\0338";
	char pos_str[] = "\033[XX;XXXH";
	str_copy(prefix, out);

	pos_str[2] = (row / 10) + '0';
	pos_str[3] = (row % 10) + '0';
	pos_str[5] = (col / 100) + '0';
	pos_str[6] = ((col / 10) % 10) + '0';
	pos_str[7] = (col % 10) + '0';
	str_cat(out, pos_str);
	str_cat(out, "   ");
	str_cat(out, pos_str);
	str_cat(out, track_nodes[track_idx].name);
	str_cat(out, suffix);
	prints(out);
}

void _update_train_ui(struct Train_Info train_metadata[6]) {
	int speed_y = 8, vel_y = 9, loc_y = 10, time_y = 12, dist_y = 13, dest_y = 14, cur_dest_y = 11;
	int speed_x = 9, vel_x = 19, loc_x = 12, time_x = 10, dist_x = 14, dest_x = 15, cur_dest_x = 15;
	for (int i = 0; i < 6; i++) {
		int offset = (24 * i) + 40;
		// speed
		_print_ui_int(speed_y, speed_x + offset, train_metadata[i].train_speed);
		// velocity
		for (int j = 0; j < 3; j++) {
			if (train_metadata[i].train_speed == g_train_speeds[i][j]) {
				_print_ui_int(vel_y, vel_x + offset, train_metadata[i].train_velocity[j]);
			}
		}
		if (train_metadata[i].train_speed == 0) {
			_print_ui_int(vel_y, vel_x + offset, 0);
		}

		// location
		if (train_metadata[i].cur_loc) {
			ASSERT(train_metadata[i].cur_loc);
			_print_ui_sensor(loc_y, loc_x + offset, train_metadata[i].cur_loc->idx);
		}
		// time error
		_print_ui_int(time_y, time_x + offset, train_metadata[i].time_error);
		// distance error
		_print_ui_int(dist_y, dist_x + offset, train_metadata[i].dist_error);
		// print destination
		_print_ui_sensor(dest_y, dest_x + offset, train_metadata[i].cur_dest);
		// print current (possibly intermediate) dest
		_print_ui_sensor(cur_dest_y, cur_dest_x + offset, train_metadata[i].parking_spot);
	}
}

void back_up_train(int clock_server_tid, struct Train_Info train) {
	// time to back up calculate 10cm~ish at low speed
	uint16_t time_offset = 50; // adjust for delay it takes for speed command to execute
	// time in ticks
	uint16_t back_up_distance = 150;																// mm
	uint32_t ticks_to_back_up = ((back_up_distance * 100) / train.train_velocity[0]) + time_offset; // scuffed
	// debug_printi("TICKS TO BACK UP: ", ticks_to_back_up, "\r\n");
	Delay(clock_server_tid, 10);
	reverse_instantly(train.train_num);
	Delay(clock_server_tid, 10);
	send_speed_command(train.train_num, g_train_speeds[train_num_to_index(train.train_num)][0]);
	Delay(clock_server_tid, ticks_to_back_up);
	// debug_prints("STOPPING AFTER BACKUP\r\n");
	send_speed_command(train.train_num, 0);
	Delay(clock_server_tid, 40);
	// debug_prints("REVERSING INSTANTLY\r\n");
	reverse_instantly(train.train_num); // this will delay 400ms by itself
}

struct start_path_request {
	struct Train_Info train_metadata;
	int possibly_new_dest;
	int train_start_time;
};

void _start_train_path() {
	int sender;
	char reply[1] = { 0 };
	char rcv_buf[sizeof(struct start_path_request)];
	Receive(&sender, rcv_buf, sizeof(struct start_path_request));
	Reply(sender, reply, 1);
	// debug_prints("EXECUTING PATH START LOGIC\r\n");

	int track_admin_tid = get_tid_by_name(TRACK_ADMIN);
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	struct start_path_request* req = (struct start_path_request*)rcv_buf;

	// debug_printi("Parking info for train: ", req->train_metadata.train_num, "\r\n");
	// park_train(req->possibly_new_dest, track_admin_tid);

	// Back up train if nessecary
	ASSERT(req->train_metadata.cur_loc);
	if (is_branch_ahead(req->train_metadata.cur_loc->idx)) {
		// debug_prints("Found Branch ahead of cur_loc, backing up\r\n");
		back_up_train(clock_server_tid, req->train_metadata);
		// Start the train at the time the pathfinding expected it to
		// debug_printi("DONE BACKING UP, STARTING TRAIN AT: ", req->train_start_time, "\r\n");
		DelayUntil(clock_server_tid, req->train_start_time);
		// debug_prints("Starting from backed up location\r\n");
	}

	flip_next_switches(&req->train_metadata.path);

	// unpark_train(req->train_metadata.parking_spot, track_admin_tid);

	/*
	Short move logic:
		1. A short move is defined as a path that has 1 or 2 sensors on it (including the beginning and end
	sensors)
		2. We require the train to hit at least 1 sensor
	*/
	if (sensors_left(&req->train_metadata.path) <= 2) {
		// debug_prints("Short move: going on low speed\r\n");
		send_speed_command(req->train_metadata.train_num,
						   g_train_speeds[train_num_to_index(req->train_metadata.train_num)][0]);
	} else {
		// debug_prints("Going on medium speed\r\n");
		send_speed_command(req->train_metadata.train_num,
						   g_train_speeds[train_num_to_index(req->train_metadata.train_num)][1]);
	}

	Exit();
}

void start_path(struct Train_Info train_metadata, int possibly_new_dest, int train_start_time) {
	struct start_path_request req = { .train_metadata = train_metadata,
									  .possibly_new_dest = possibly_new_dest,
									  .train_start_time = train_start_time };
	char reply[1];
	int child_id = Create(2, _start_train_path);
	Send(child_id, (char*)&req, sizeof(struct start_path_request), reply, 1);
}

struct random_path_request {
	uint8_t train_num;
	uint8_t cur_loc;
};

void _pathfind_courier() {
	int sender;
	char reply[1];
	char rcvbuf[sizeof(struct random_path_request)];
	struct random_path_request* req = (struct random_path_request*)rcvbuf;
	Receive(&sender, rcvbuf, sizeof(struct random_path_request));
	Reply(sender, reply, 1);

	// Delay(get_tid_by_name(CLOCK_SERVER), 100);
	uint8_t dest = get_random_sensor();
	while (dest == req->cur_loc) {
		dest = get_random_sensor();
	}
	set_path2(req->train_num, dest, 0);
	Exit();
}

void set_random_path(uint8_t train_num, uint8_t cur_loc) {
	char reply[1];
	int child_tid = Create(3, _pathfind_courier);
	struct random_path_request req = { .train_num = train_num, .cur_loc = cur_loc };
	Send(child_tid, (char*)&req, sizeof(struct random_path_request), reply, 1);
}

int can_reach_sensor_ahead(int cur_node_idx, uint32_t blocked[144][6][2], uint32_t curtime, uint32_t velocity) {
	// assuming cur_node_idx is sensor node
	struct track_node* next = &track_nodes[cur_node_idx];
	char switch_state;
	uint32_t dist = 0;
	debug_prints("can reach sensor ahead start\r\n");
	do {
		// transition
		if (next->type == NODE_BRANCH) {
			switch_state = get_switch_state(next->num);
			ASSERT(switch_state == 'C' || switch_state == 'S');
			if (switch_state == 'C') {
				next = next->edge[DIR_CURVED].dest;
				dist += next->edge[DIR_CURVED].dist;
			} else {
				next = next->edge[DIR_STRAIGHT].dest;
				dist += next->edge[DIR_STRAIGHT].dist;
			}
		} else {
			next = next->edge[DIR_AHEAD].dest;
			dist += next->edge[DIR_AHEAD].dist;
		}
	} while (next->type != NODE_SENSOR);
	// return parking_spot_is_blocked(next->idx, blocked, cur_node_idx, Time(get_tid_by_name(CLOCK_SERVER)))
	// 		   ? 0
	// 		   : (1 + next->idx);
	debug_printi("Found sensor ahead: ", next->idx, "\r\n");

	return is_section_blocked2(velocity, blocked, next->idx, dist, curtime, cur_node_idx) ? 0 : (next->idx + 1);
}

void train_admin() {
	RegisterAs(TRAIN_ADMIN);

	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	int track_admin_tid = get_tid_by_name(TRACK_ADMIN);
	uint32_t curtime;

	int run_random_paths = 0;

	int user_train = 24;
	int chase_train = 78;
	// train metadata
	char cur_train = 24;
	int cur_train_ind = 2;
	struct Train_Info train_metadata[6];
	_init_train_ui(clock_server_tid);
	init_train_metadata(train_metadata);
	uint32_t blocked[144][6][2];

	// for msg passing
	int sender;
	char rcvbuf[sizeof(struct Train_Admin_Request)];
	char reply = 'R';
	int child_tid;

	struct Calibration_Request calibration_request;

	// for train calibration
	char calibrating_train = 0;
	int calibration_task_tid = 0;

	char user_command;
	int user_train_ind;
	uint8_t user_train_next_branch;
	char user_train_switch_num;
	char direction;

	for (;;) {
		// status:
		// green: ready
		// red: sensor update
		// yellow: path update
		// blue: try to start path
		prints("\0337\033[1;40H\033[32mTC\033[0m\0338");
		Receive(&sender, rcvbuf, sizeof(struct Train_Admin_Request));

		curtime = Time(clock_server_tid);
		// prints("\0337\033[1;40H\033[31mTC\033[0m\0338");
		struct Train_Admin_Request* req = (struct Train_Admin_Request*)rcvbuf;
		if (req->request_type != TA_TRY_TO_START_PATH) {
			Reply(sender, &reply, 1);
		}

		switch (req->request_type) {
		case TA_USER_TRAIN_COMMAND:
			user_command = req->update;
			user_train_ind = train_num_to_index(user_train);
			if (user_command == 'W') {
				debug_prints("User hit W\r\n");

				if (train_metadata[user_train_ind].direction == BACKWARDS) {
					if (train_metadata[user_train_ind].train_speed == 0) {
						// reverse instantly and go forwards direction
						reverse_instantly(user_train);
						train_metadata[user_train_ind].direction = FORWARDS;
					} else {
						send_speed_command(user_train, 0);
					}
				} else {
					send_speed_command(user_train, g_train_speeds[user_train_ind][1]);
				}
			} else if (user_command == 'A') {
				debug_prints("User hit A\r\n");
				// left
				ASSERT(train_metadata[user_train_ind].cur_loc);
				user_train_next_branch = next_branch_from_sensor(train_metadata[user_train_ind].cur_loc->idx);
				if (user_train_next_branch == 0xFF) {
					debug_prints("no branches ahead\r\n");
					break;
				}
				user_train_switch_num = track_nodes[user_train_next_branch].num;
				// TODO use g_branch_info_b and update b to correct directions
				ASSERT(g_branch_info_a[(int)user_train_switch_num - 1][0] == user_train_switch_num);
				direction = g_branch_info_a[(int)user_train_switch_num - 1][1] == STRAIGHT_IS_LEFT ? 'S' : 'C';
				turn_switches(&user_train_switch_num, &direction, 1);
			} else if (user_command == 'S') {
				debug_prints("User hit S\r\n");
				if (train_metadata[user_train_ind].direction == FORWARDS) {
					if (train_metadata[user_train_ind].train_speed == 0) {
						// reverse instantly and go in backwards direction
						reverse_instantly(user_train);
						train_metadata[user_train_ind].direction = BACKWARDS;
					} else {
						send_speed_command(user_train, 0);
					}
				} else {
					send_speed_command(user_train, g_train_speeds[user_train_ind][1]);
				}
			} else if (user_command == 'D') {
				debug_prints("User hit D\r\n");
				ASSERT(train_metadata[user_train_ind].cur_loc);
				user_train_next_branch = next_branch_from_sensor(train_metadata[user_train_ind].cur_loc->idx);

				if (user_train_next_branch == 0xFF) {
					debug_prints("no branches ahead\r\n");
					break;
				}
				user_train_switch_num = track_nodes[user_train_next_branch].num;
				ASSERT(g_branch_info_a[(int)user_train_switch_num - 1][0] == user_train_switch_num);
				direction = g_branch_info_a[(int)user_train_switch_num - 1][1] == STRAIGHT_IS_LEFT ? 'C' : 'S';

				turn_switches(&user_train_switch_num, &direction, 1);
			} else {
				ASSERT(0);
			}
			break;
		case TA_CALIBRATION_BEGIN:
			if (calibrating_train) {
				debug_printi("Already calibrating train ", calibrating_train, ", please try again later\r\n");
				continue;
			}
			// begin calibration
			calibrating_train = req->train_num;
			calibration_request.request_type = CR_START;
			calibration_request.value = calibrating_train;
			calibration_task_tid = Create(1, train_calibration_task);
			Send(calibration_task_tid, (char*)&calibration_request, sizeof(struct Calibration_Request), &reply, 1);
			break;
		case TA_CALIBRATION_END:
			if (!calibrating_train) {
				ASSERT(0);
			}
			// end calibration
			debug_printi("Finished calibrating train ", calibrating_train, "\r\n");
			calibrating_train = 0;
			break;
		case TA_CALIBRATION_VELOCITY_HI:
			debug_printi("Got calibrated high velocity: ", req->update, "mm/s\r\n");
			train_metadata[train_num_to_index(req->train_num)].train_velocity[2] = req->update;
			break;
		case TA_CALIBRATION_VELOCITY_MED:
			debug_printi("Got calibrated medium velocity: ", req->update, "mm/s\r\n");
			train_metadata[train_num_to_index(req->train_num)].train_velocity[1] = req->update;
			break;
		case TA_CALIBRATION_VELOCITY_LO:
			debug_printi("Got calibrated low velocity: ", req->update, "mm/s\r\n");
			train_metadata[train_num_to_index(req->train_num)].train_velocity[0] = req->update;
			break;
		case TA_CALIBRATION_STOPPING_DISTANCE:
			debug_printi("Got calibrated stopping distance: ", req->update, "mm\r\n");
			// TODO: Figure out why we off by factor of 10 and remove this /10
			train_metadata[train_num_to_index(req->train_num)].stopping_distance = req->update;
			break;
		case TA_SET_TRAIN:
			cur_train_ind = train_num_to_index(req->train_num);
			cur_train = req->train_num;
			debug_printi("Set current train to index: ", cur_train_ind, "\r\n");
			break;
		case TA_SPEED_UPDATE:
			/* code */
			train_metadata[train_num_to_index(req->train_num)].train_speed = req->update;
			train_metadata[train_num_to_index(req->train_num)].stopping = 0;
			break;
		case TA_RNG:
			if (run_random_paths) {
				run_random_paths = 0;
			} else {
				run_random_paths = 1;
			}
			break;
		case TA_SENSOR_UPDATE:
			// If we're calibrating a train, need to forward this update to our calibration task
			if (calibrating_train) {
				calibration_request.request_type = CR_SENSOR;
				calibration_request.value = req->update;
				Send(calibration_task_tid, (char*)&calibration_request, sizeof(struct Calibration_Request), &reply, 1);
			}
			// determine which train triggered the sensor
			cur_train = check_which_train_triggered_sensor(train_metadata, req->update);
			if (!cur_train) {
				break;
			}

			// under here because it has an assertion for invalid train
			cur_train_ind = train_num_to_index(cur_train);

			// 1. update the current position
			train_metadata[cur_train_ind].prev_loc = train_metadata[cur_train_ind].cur_loc;
			train_metadata[cur_train_ind].cur_loc = &track_nodes[req->update];

			update_sensor_attribution(train_metadata, cur_train_ind);
			// debug_printi("", count, " possible sensors next\r\n");

			// figure out train 24 actions (final project)
			if (cur_train == user_train) {
				get_reserved_segments(blocked);
				// 1. look ahead 1 sensor based on switch state
				//    1a. If train on ahead node, stop (send 0 immediately)
				ASSERT(train_metadata[cur_train_ind].cur_loc);
				int next_sensor_id = can_reach_sensor_ahead(train_metadata[cur_train_ind].cur_loc->idx,
															blocked,
															curtime,
															train_metadata[cur_train_ind].train_velocity[1]);
				if (next_sensor_id) {
					debug_printi("Sensor ahead of Train 24:", next_sensor_id - 1, "\r\n");
					park_train(next_sensor_id - 1, track_admin_tid);
					unpark_train(train_metadata[cur_train_ind].cur_loc->idx, track_admin_tid);
				} else {
					debug_prints("Sensor ahead of Train 24 Blocked\r\n");
					send_speed_command(user_train, 0);
					send_speed_command(chase_train, 0);
					honk();
				}
			}

			if (train_metadata[cur_train_ind].has_path) {
				update_path(&train_metadata[cur_train_ind].path, train_metadata[cur_train_ind].cur_loc, cur_train);
				flip_next_switches(&train_metadata[cur_train_ind].path);
			}

			// 2. maybe stop the train
			if (train_metadata[cur_train_ind].has_path && !train_metadata[cur_train_ind].stopping) {
				uint64_t dist_to_dest
					= distance_to_dest(&train_metadata[cur_train_ind].path, train_metadata[cur_train_ind].cur_loc)
					  + train_metadata[cur_train_ind].offset;

				int sensors_to_dest = sensors_left(&train_metadata[cur_train_ind].path);

				handle_possible_stop(train_metadata, sensors_to_dest, dist_to_dest, curtime, cur_train, cur_train_ind);
			}

			// 3. if we hit all the nodes, stop all actions
			if (train_metadata[cur_train_ind].has_path && ring_buffer_is_empty(&train_metadata[cur_train_ind].path)) {
				train_metadata[cur_train_ind].has_path = 0;

				if (train_metadata[cur_train_ind].reversing) {
					debug_prints("reversing train\n");
					ASSERT(train_metadata[cur_train_ind].cur_loc);
					train_metadata[cur_train_ind].cur_loc = train_metadata[cur_train_ind].cur_loc->reverse;
					train_metadata[cur_train_ind].reversing = 0;
					// rev immediate
					reverse_instantly(cur_train);
					update_sensor_attribution(train_metadata, cur_train_ind);
				}

				debug_printi("Finished path for train: ", cur_train, "\r\n");
				// debug_printi("cur_loc: ", train_metadata[cur_train_ind].cur_loc->idx, "\r\n");
				// debug_printi("cur_dest: ", train_metadata[cur_train_ind].cur_dest, "\r\n");
				ASSERT(train_metadata[cur_train_ind].cur_loc);
				if (train_metadata[cur_train_ind].cur_loc->idx != train_metadata[cur_train_ind].cur_dest) {
					child_tid = Create(3, try_to_start_path);
					struct Train_Admin_Request new_path_req = { .request_type = TA_PATH_UPDATE,
																.train_num = cur_train,
																.src = train_metadata[cur_train_ind].cur_loc->idx,
																.dest = train_metadata[cur_train_ind].cur_dest,
																.offset = train_metadata[cur_train_ind].offset };
					Send(child_tid, (char*)&new_path_req, sizeof(struct Train_Admin_Request), &reply, 1);
				} else if (run_random_paths) {
					debug_printi("SETTING NEW RANDOM PATH FOR TRAIN: ", cur_train, "\r\n");
					set_random_path(cur_train, train_metadata[cur_train_ind].cur_loc->idx);
				}
			}

			// 4. Calculate the current velocity with ewma
			if (train_metadata[cur_train_ind].has_path && train_metadata[cur_train_ind].last_sensor_trigger_time != 0) {
				// TODO: this node distance call is scuffed, change it
				// update velocity of train with alpha denom
				// next sensor distance is distance to current node right now
				uint32_t alpha = train_metadata[cur_train_ind].alpha_denom;
				int old_velocity;
				int new_velocity = (train_metadata[cur_train_ind].next_sensor_distance * 100)
								   / (curtime - train_metadata[cur_train_ind].last_sensor_trigger_time);
				// calculate the velocity based on current speed (has to be valid speed)
				int updated_velocity = 0;
				for (int i = 0; i < 3; i++) {
					if (train_metadata[cur_train_ind].train_speed == g_train_speeds[cur_train_ind][i]) {
						old_velocity = train_metadata[cur_train_ind].train_velocity[i];
						train_metadata[cur_train_ind].train_velocity[i]
							= (old_velocity >> alpha) * ((1 << alpha) - 1) + (new_velocity >> alpha);
						updated_velocity = 1;
						// debug_printi("New EWMA Velocity: ", train_metadata[cur_train_ind].train_velocity[i], "\r\n");
					}
				}
				if (!updated_velocity && train_metadata[cur_train_ind].train_speed > 0) {
					debug_prints("ERROR: TRAIN IS ON INVALID SPEED PLEASE CHANGE IT\r\n");
				}
			}

			// 5. Calculate prediction errors
			if (train_metadata[cur_train_ind].has_path
				&& train_metadata[cur_train_ind].predicted_next_sensor_time > 0) {
				int actual_time_to_sensor = curtime - train_metadata[cur_train_ind].last_sensor_trigger_time;
				int time_error = train_metadata[cur_train_ind].predicted_time_to_sensor - actual_time_to_sensor;

				int distance_error = 0;
				for (int i = 0; i < 3; i++) {
					if (train_metadata[cur_train_ind].train_speed == g_train_speeds[cur_train_ind][i]) {
						distance_error = ((int)(time_error * train_metadata[cur_train_ind].train_velocity[i])) / 100;
					}
				}

				// TODO: move this to its own section of the screen
				// char prefix[] = "Sensor:  ";
				// prefix[8] = 'A' + (req->update) / 16;
				// debug_printi(prefix, (req->update % 16) + 1, "\r\n");
				// debug_printi_maybe_negative("Prediction was off by: ", time_error, "ticks \r\n");
				// debug_printi_maybe_negative("Distance error: ", distance_error, "mm \r\n");
				train_metadata[cur_train_ind].time_error = time_error;
				train_metadata[cur_train_ind].dist_error = distance_error;
			}

			// 6. predict next sensor and time to arrive at next sensor
			if (train_metadata[cur_train_ind].has_path) {
				int distance = distance_to_next_sensor_on_path(&train_metadata[cur_train_ind].path,
															   train_metadata[cur_train_ind].cur_loc); // mm
				train_metadata[cur_train_ind].predicted_next_sensor_time = 0;
				train_metadata[cur_train_ind].predicted_time_to_sensor = 0;
				if (distance != 0) {
					// TODO: make this dynamic
					// mm / (mm/s) = s, s*100 -> ticks
					if (train_metadata[cur_train_ind].train_speed == g_train_speeds[cur_train_ind][0]) {
						train_metadata[cur_train_ind].predicted_time_to_sensor
							= (distance * 100) / train_metadata[cur_train_ind].train_velocity[0];
					} else if (train_metadata[cur_train_ind].train_speed == g_train_speeds[cur_train_ind][1]) {
						train_metadata[cur_train_ind].predicted_time_to_sensor
							= (distance * 100) / train_metadata[cur_train_ind].train_velocity[1];
					} else if (train_metadata[cur_train_ind].train_speed == g_train_speeds[cur_train_ind][2]) {
						train_metadata[cur_train_ind].predicted_time_to_sensor
							= (distance * 100) / train_metadata[cur_train_ind].train_velocity[2];
					}

					train_metadata[cur_train_ind].predicted_next_sensor_time
						= curtime + train_metadata[cur_train_ind].predicted_time_to_sensor;
				}
			}

			// 7. update last sensor hit time and next sensor distance
			if (train_metadata[cur_train_ind].has_path) {

				train_metadata[cur_train_ind].last_sensor_trigger_time = curtime;
				train_metadata[cur_train_ind].next_sensor_distance
					= distance_to_next_sensor_on_path(&train_metadata[cur_train_ind].path,
													  train_metadata[cur_train_ind].cur_loc); // mm
			}
			ASSERT(train_metadata[cur_train_ind].cur_loc);
			update_track_ui(TRACK_UI_SENSOR_TRAIN, train_metadata[cur_train_ind].cur_loc->idx, cur_train);
			break;
		case TA_PATH_UPDATE:
			if (!is_legal_train_num(req->train_num)) {
				debug_printi("Got illegal train num for command: ", req->train_num, "\r\n");
				break;
			}
			cur_train_ind = train_num_to_index(req->train_num);
			child_tid = Create(3, try_to_start_path);
			ASSERT(train_metadata[cur_train_ind].cur_loc);
			req->src = train_metadata[train_num_to_index(req->train_num)].cur_loc->idx;
			// debug_printi("Got path update for train: ", req->train_num, "\r\n");
			// debug_printi("PATHING FROM NODE WITH INDEX: ", req->src, "\r\n");
			// debug_printi("PATHING TO NODE WITH INDEX: ", req->dest, "\r\n");
			train_metadata[cur_train_ind].cur_dest = req->dest;
			Send(child_tid, (char*)req, sizeof(struct Train_Admin_Request), &reply, 1);
			break;
		case TA_TRY_TO_START_PATH:
			// blue
			prints("\0337\033[1;40H\033[34mTC\033[0m\0338");
			cur_train_ind = train_num_to_index(req->train_num);
			cur_train = req->train_num;
			train_metadata[cur_train_ind].path = create_buffer(CHAR);
			get_reserved_segments(blocked);
			// if we are in front of a branch:
			// 1. Compute Dijkstra_tc2 for X seconds in the future
			// 2. reverse instantly
			// 3. travel 10cm
			// 4. reverse instantly
			// 5. Delay until curtime + X
			// 6. Start the path
			int train_start_time = Time(clock_server_tid) + 100; // base offset to account for acceleration
			// use backup flag to signal dijkstras the train will start ~1 second behind sensor
			uint8_t need_back_up = 0;
			ASSERT(train_metadata[cur_train_ind].cur_loc);
			if (is_branch_ahead(train_metadata[cur_train_ind].cur_loc->idx)) {
				train_start_time += BACK_UP_TIME;
				need_back_up = 1;
			}
			// yellow
			prints("\0337\033[1;40H\033[33mTC\033[0m\0338");
			uint8_t possibly_new_dest = dijkstra_tc2(req->src,
													 req->dest,
													 train_metadata[cur_train_ind].train_velocity[1],
													 need_back_up ? train_start_time + 100 : train_start_time,
													 blocked,
													 train_metadata[cur_train_ind].parking_spot,
													 &train_metadata[cur_train_ind].path,
													 &(train_metadata[cur_train_ind].reversing));
			// red
			prints("\0337\033[1;40H\033[31mTC\033[0m\0338");
			if (train_metadata[cur_train_ind].reversing && train_metadata[cur_train_ind].path.size == 1) {
				debug_prints("reversing train in place\n");
				ASSERT(train_metadata[cur_train_ind].cur_loc);
				train_metadata[cur_train_ind].cur_loc = train_metadata[cur_train_ind].cur_loc->reverse;
				train_metadata[cur_train_ind].reversing = 0;
				// rev immediate
				reverse_instantly(cur_train);
				update_sensor_attribution(train_metadata, cur_train_ind);

				if (train_metadata[cur_train_ind].cur_loc->idx != train_metadata[cur_train_ind].cur_dest) {
					child_tid = Create(3, try_to_start_path);
					struct Train_Admin_Request new_path_req = { .request_type = TA_PATH_UPDATE,
																.train_num = cur_train,
																.src = train_metadata[cur_train_ind].cur_loc->idx,
																.dest = train_metadata[cur_train_ind].cur_dest,
																.offset = train_metadata[cur_train_ind].offset };
					Send(child_tid, (char*)&new_path_req, sizeof(struct Train_Admin_Request), &reply, 1);
				} else if (run_random_paths) {
					debug_printi("SETTING NEW RANDOM PATH FOR TRAIN: ", cur_train, "\r\n");
					set_random_path(cur_train, train_metadata[cur_train_ind].cur_loc->idx);
				}
				reply = 1;
				Reply(sender, &reply, 1);
				break;
			}

			if (possibly_new_dest == 0xFF) {
				// path is impossible
				reply = 3;
				Reply(sender, &reply, 1);
				ASSERT(train_metadata[cur_train_ind].cur_loc);
				train_metadata[cur_train_ind].cur_dest = train_metadata[cur_train_ind].cur_loc->idx;
				break;
			}

			if (ring_buffer_is_empty(&train_metadata[cur_train_ind].path)
				|| train_metadata[cur_train_ind].path.size == 1) {
				reply = 0;
				Reply(sender, &reply, 1);
				break;
			}
			reply = 1;
			Reply(sender, &reply, 1);

			start_path(train_metadata[cur_train_ind], possibly_new_dest, train_start_time);
			train_metadata[cur_train_ind].parking_spot = possibly_new_dest;
			train_metadata[cur_train_ind].has_path = 1;
			train_metadata[cur_train_ind].last_sensor_trigger_time = 0;
			train_metadata[cur_train_ind].predicted_next_sensor_time = 0;
			train_metadata[cur_train_ind].predicted_time_to_sensor = 0;
			train_metadata[cur_train_ind].offset = req->offset;
			train_metadata[cur_train_ind].prev_loc = NULL;
			train_metadata[cur_train_ind].cur_loc = NULL;
			break;
		case TA_LOOP_STOP:
			debug_printi("Stopping train at ", req->dest, "\r\n");
			// assume that our cur_loc is always up to date (i.e we've been running in the loop for at least 1
			// sensor) trying_to_loop_stop = 1; loop_stop_dest = req->dest;
			break;
		case TA_LOCATE_TRAIN:
			debug_printi("Locating train ", req->train_num, "\r\n");
			cur_train_ind = train_num_to_index(req->train_num);
			train_metadata[cur_train_ind].cur_loc = &track_nodes[req->update];
			train_metadata[cur_train_ind].direction = 0;
			update_sensor_attribution(train_metadata, cur_train_ind);
			ASSERT(train_metadata[cur_train_ind].cur_loc);
			update_track_ui(TRACK_UI_SENSOR_TRAIN, train_metadata[cur_train_ind].cur_loc->idx, req->train_num);
			break;
		default:
			break;
		}
		_update_train_ui(train_metadata);
	}

	ASSERT(0);
	Exit();
}

/**********************************************/
/************* Callibration tasks *************/
/**********************************************/
void set_path_cal(uint8_t src, uint8_t dest) {
	struct Ring_Buffer path = create_buffer(CHAR);
	char switches_to_turn[22];
	char switch_directions[22];
	uint8_t sw_count = 0;

	dijkstra(src, dest, &path);

	while (!ring_buffer_is_empty(&path)) {
		uint8_t node_idx = ring_buffer_pop(&path);
		struct track_node node = track_nodes[node_idx];

		if (node.type == NODE_BRANCH && !ring_buffer_is_empty(&path)) {
			if (node.edge[DIR_STRAIGHT].dest->idx == path.cbuffer[path.l]) {
				switches_to_turn[sw_count] = (char)node.num;
				switch_directions[sw_count] = 'S';
			} else if (node.edge[DIR_CURVED].dest->idx == path.cbuffer[path.l]) {
				switches_to_turn[sw_count] = (char)node.num;
				switch_directions[sw_count] = 'C';
			} else {
				prints("FAILED ON: ");
				prints((char*)node.name);
				prints(" NEXT NODE IN PATH: ");
				prints((char*)track_nodes[(uint8_t)path.cbuffer[path.l]].name);
				prints("\r\n");
				ASSERT(0);
			}
			sw_count++;
		}
	}
	ASSERT(sw_count < 23);

	turn_switches(switches_to_turn, switch_directions, sw_count);
}

void calibrate_train(char train) {
	char reply;
	struct Train_Admin_Request req = { .request_type = TA_CALIBRATION_BEGIN, .train_num = train, .update = 0 };
	Send(get_tid_by_name(TRAIN_ADMIN), (char*)&req, sizeof(struct Train_Admin_Request), &reply, 1);
}

int calibration_task_get_sensor_read(char* rcvbuf) {
	int sender_tid;
	char reply = 'R';
	Receive(&sender_tid, rcvbuf, sizeof(struct Calibration_Request));
	Reply(sender_tid, &reply, 1);
	struct Calibration_Request* req = (struct Calibration_Request*)rcvbuf;
	return req->value;
}

// returns the loop distance
int calibration_task_setup_track() {
	uint8_t sensor_A4 = (('A' - 'A') * 16) + 4 - 1;
	uint8_t sensor_E9 = (('E' - 'A') * 16) + 9 - 1;
	uint8_t sensor_A3 = (('A' - 'A') * 16) + 3 - 1;
	uint8_t sensor_E10 = (('E' - 'A') * 16) + 10 - 1;
	set_path_cal(sensor_A3, sensor_E10);
	set_path_cal(sensor_E10, sensor_A3);
	set_path_cal(sensor_A4, sensor_E9);
	set_path_cal(sensor_E9, sensor_A4);
	struct Ring_Buffer _useless = create_buffer(CHAR);
	int distance_1 = dijkstra(sensor_A4, sensor_E9, &_useless);
	int distance_2 = dijkstra(sensor_E9, sensor_A4, &_useless);

	int total_loop_distance = distance_1 + distance_2;
	return total_loop_distance;
}

// stopping distance is for med_vel to 0
void send_calibration_data_to_train_admin(
	int train_num, int stopping_distance, int low_vel, int med_vel, int high_vel) {
	char reply;
	int train_admin_tid = get_tid_by_name(TRAIN_ADMIN);

	struct Train_Admin_Request metadata_update;
	metadata_update.request_type = TA_CALIBRATION_STOPPING_DISTANCE;
	metadata_update.train_num = train_num;
	metadata_update.update = stopping_distance;
	Send(train_admin_tid, (char*)&metadata_update, sizeof(struct Train_Admin_Request), &reply, 1);

	metadata_update.request_type = TA_CALIBRATION_VELOCITY_HI;
	metadata_update.train_num = train_num;
	metadata_update.update = high_vel;
	Send(train_admin_tid, (char*)&metadata_update, sizeof(struct Train_Admin_Request), &reply, 1);

	metadata_update.request_type = TA_CALIBRATION_VELOCITY_MED;
	metadata_update.train_num = train_num;
	metadata_update.update = med_vel;
	Send(train_admin_tid, (char*)&metadata_update, sizeof(struct Train_Admin_Request), &reply, 1);

	metadata_update.request_type = TA_CALIBRATION_VELOCITY_LO;
	metadata_update.train_num = train_num;
	metadata_update.update = low_vel;
	Send(train_admin_tid, (char*)&metadata_update, sizeof(struct Train_Admin_Request), &reply, 1);
}

int calculate_stopping_distance(
	int clock_server_tid, int train_num, int med_speed, char* rcvbuf, int low_speed, int low_vel, int high_speed) {
	// stopping distance calculation
	Delay(clock_server_tid, 500);
	// send_speed_command(train_num, high_speed);
	// need to read 5 here because the train can roll over at most 2 sensors when stopping from previous
	// calibration, those sensors don't get read and get sent here for (int i = 0; i < 5; i++) {
	// calibration_task_get_sensor_read(rcvbuf);
	// }
	(void)high_speed;

	send_speed_command(train_num, med_speed);
	for (int i = 0; i < 4; i++) {
		calibration_task_get_sensor_read(rcvbuf);
	}

	int start_sensor = calibration_task_get_sensor_read(rcvbuf);
	send_speed_command(train_num, 0);
	Delay(clock_server_tid, 500);

	int start_slow_time = Time(clock_server_tid);
	send_speed_command(train_num, low_speed);

	int next_sensor = calibration_task_get_sensor_read(rcvbuf);
	int end_slow_time = Time(clock_server_tid);
	// we need to at least read one more sensor than the ones the train hit while stopping (otherwise this
	// calculation will be off)
	for (int i = 0; i < 4; i++) {
		next_sensor = calibration_task_get_sensor_read(rcvbuf);
		end_slow_time = Time(clock_server_tid);
	}

	send_speed_command(train_num, 0);
	int stopping_distance
		= (node_distance(start_sensor, next_sensor) * 1000) - ((low_vel * (end_slow_time - start_slow_time)) / 100);
	debug_printi_maybe_negative("stopping distance: ", stopping_distance, "μm\r\n");
	return stopping_distance;
}

void train_calibration_task() {
	int sender_tid;
	char rcvbuf[sizeof(struct Calibration_Request)];
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	char reply = 'R';

	// initial calibration request telling us what train to calibrate
	Receive(&sender_tid, rcvbuf, sizeof(struct Calibration_Request));
	Reply(sender_tid, &reply, 1);
	struct Calibration_Request* req = (struct Calibration_Request*)rcvbuf;
	char train_num = req->value;

	ASSERT(req->request_type == CR_START);
	train_num_to_index(train_num); // to assert the given train number if valid

	debug_printi("Calibrating train ", train_num, "\r\n");
	// set the main loop, train must be on this path
	int total_loop_distance = calibration_task_setup_track();

	const char* speeds = g_train_speeds[train_num_to_index(train_num)];
	uint32_t velocities[] = { 0, 0, 0 };
	// Speed calibrations
	int loop_sensor_times[14]; // 13 to get back to start, throw out first one to get up to speed
	int total_time_around_track, velocity_around_track, speed;
	for (int j = 0; j < 3; j++) {
		speed = speeds[j];
		send_speed_command(train_num, speed);
		for (int i = 0; i < 14; i++) {
			calibration_task_get_sensor_read(rcvbuf);
			loop_sensor_times[i] = Time(clock_server_tid);
		}
		send_speed_command(train_num, 0);
		total_time_around_track = (loop_sensor_times[13] - loop_sensor_times[1]);
		debug_printi("Total time around track: ", total_time_around_track, " ticks\r\n");
		velocity_around_track = (total_loop_distance * 100000) / total_time_around_track;
		velocities[j] = velocity_around_track;
		debug_printi("Velocity: ", velocity_around_track, "μm/s \r\n");
	}

	// stopping distance calculation
	int stopping_distance1 = calculate_stopping_distance(
								 clock_server_tid, train_num, speeds[1], rcvbuf, speeds[0], velocities[0], speeds[2])
							 / 1000;
	int stopping_distance2 = calculate_stopping_distance(
								 clock_server_tid, train_num, speeds[1], rcvbuf, speeds[0], velocities[0], speeds[2])
							 / 1000;
	int stopping_distance = (stopping_distance1 + stopping_distance2) / 2;

	// send data back to train_admin
	send_calibration_data_to_train_admin(
		train_num, stopping_distance, velocities[0] / 1000, velocities[1] / 1000, velocities[2] / 1000);

	// end calibration
	struct Train_Admin_Request end_req = { .request_type = TA_CALIBRATION_END };
	Send(get_tid_by_name(TRAIN_ADMIN), (char*)&end_req, sizeof(struct Train_Admin_Request), &reply, 1);
	Exit();
}

void control_user_train(char command) {
	struct Train_Admin_Request req = { .request_type = TA_USER_TRAIN_COMMAND, .update = command };
	char reply;
	Send(get_tid_by_name(TRAIN_ADMIN), (char*)&req, sizeof(struct Train_Admin_Request), &reply, 1);
}