#ifndef TRACK_ADMIN_H
#define TRACK_ADMIN_H
#include "request.h"
#include "syscall.h"
#include "util/util.h"

enum track_request {
	TRACK_UPDATE_SWITCH,
	TRACK_GET_SWITCH,
	TRACK_RESERVE_SEGMENTS,
	TRACK_GET_RESERVATIONS,
	TRACK_PARK,
	TRACK_UNPARK,
	TRACK_PRINT_RES_MAYBE,
};

struct Track_Request {
	enum track_request request_type;
	union
	{
		char switch_nums[30];
		uint8_t track_nodes_encoded[30];
	};
	union
	{
		char directions[30];
		uint32_t reservation_time[30];
	};
	union
	{
		uint8_t node_count;
		uint8_t sw_count;
	};
	// used to get switch state
	char switch_num;
};

enum track_ui_request {
	TRACK_UI_SENSOR_RESERVE,
	TRACK_UI_SENSOR_UNRESERVE,
	TRACK_UI_SENSOR_TRAIN,
};
struct Track_UI_Request {
	enum track_ui_request request_type;
	int sensor_num;
	int train_num;
};

void track_admin();
void track_ui_admin();
void flip_next_switches(struct Ring_Buffer* path);
int turn_switches(char* switch_nums, char* directions, uint8_t sw_count);
char get_switch_state(uint8_t switch_num);
// returns 1 on successfully reserving all nodes, otherwise returns 0 (fake for reserve nodes now)
int reserve_nodes(uint8_t nodes_to_reserve[50],
				  uint8_t num_nodes_to_reserve,
				  int track_admin_tid,
				  uint32_t start_time,
				  uint32_t end_time);
int unreserve_nodes(uint8_t* nodes_to_unreserve, uint8_t num_nodes_to_unreserve, int track_admin_tid);
void get_reserved_segments(uint32_t blocked[144][6][2]);

void park_train(uint8_t node, int track_admin_tid);
void unpark_train(uint8_t node, int track_admin_tid);

uint8_t get_nodes_for_parking(uint8_t node, uint8_t* buf);
int is_branch_ahead(uint8_t node_idx);

extern const uint32_t PARKING_SPOT;

uint8_t reverse_node(uint8_t node);

void update_track_ui(enum track_ui_request type, int sensor_num, int train_num);
#endif