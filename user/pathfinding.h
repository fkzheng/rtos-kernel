#ifndef PATHFINDING_H
#define PATHFINDING_H
#include "syscall.h"
#include "track_info/track.h"
#include "track_info/track_node.h"
#include "uart.h"
#include "util/ring_buffer.h"
#include "util/util.h"

struct track_path {
	uint16_t dist;
	struct track_node* cur;
	struct track_path* prev;
};

struct Ring_Buffer;
int node_distance(uint8_t src, uint8_t dest);
int dijkstra(uint8_t src, uint8_t dest, struct Ring_Buffer* path_buf);
uint8_t dijkstra_tc2(uint8_t src,
					 uint8_t dest,
					 uint16_t velocity,
					 uint32_t curtime,
					 uint32_t blocked[144][6][2],
					 uint8_t parking_spot,
					 struct Ring_Buffer* path_buf,
					 uint8_t* reverse_train);
int distance_to_next_sensor_on_path(struct Ring_Buffer* path_buf, struct track_node* cur_loc);
int sensors_left(struct Ring_Buffer* path_buf);
uint8_t next_branch_from_sensor(uint8_t node);
char parking_spot_is_blocked(uint8_t node, uint32_t blocked[144][6][2], uint8_t parking_spot, uint32_t curtime);
int is_section_blocked2(
	uint16_t velocity, uint32_t blocked[144][6][2], int node_idx, uint32_t dist, uint32_t start_time, int parking_spot);
#endif