#ifndef UART_H
#define UART_H
#include "kernel/task.h"
#include "request.h"
#include "syscall.h"

void uart_notifier();
void uart_terminal_server();
void uart_term_server_tx();
void uart_train_server_tx();
void uart_term_server_rx();
void uart_train_server_rx();
void uart_train_server_tx_no_cts();
#endif