#ifndef RANDOM_H
#define RANDOM_H

#include "request.h"
#include "syscall.h"
#include "util/util.h"

void random_server();

uint8_t get_random_sensor();
#endif