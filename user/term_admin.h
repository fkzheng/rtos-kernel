#ifndef TERM_ADMIN_H
#define TERM_ADMIN_H
#include "train_control.h"

enum command_token_type {
	COMMAND_TR,
	COMMAND_RV,
	COMMAND_SW,
	COMMAND_INT,
	COMMAND_CHAR,
	COMMAND_INVALID_TOKEN,
	COMMAND_EOL,
	COMMAND_LOOP_STOP,
	COMMAND_INIT_TRACK,
	COMMAND_SET_TRAIN,
	COMMAND_PATHFIND,
	COMMAND_CALIBRATE,
	COMMAND_RV_I,
	COMMAND_RNG,
	COMMAND_LOCATE,
};

struct Command_Token {
	enum command_token_type type;
	char value;
};

void terminal_admin();

#endif