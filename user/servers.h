#ifndef USER_SERVERS
#define USER_SERVERS
#include "kernel/task.h"
#include "request.h"
#include "syscall.h"
#include "util/ring_buffer.h"
#include "util/rpi.h"
#include "util/util.h"

extern int g_name_server_tid;
extern uint64_t g_idle_server_tid;

// for testing
void name_server();
void idle_server();
void print_idle_time_server();
#endif