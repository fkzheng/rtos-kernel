#ifndef SENSORS
#define SENSORS

#define SENSOR_MAX_QUEUE_SIZE 10  // N most recent sensor triggers to store
#define SENSOR_QUERY_INTERVAL 20  // Number of 10ms clock ticks
#define SENSOR_BYTE_NOT_READY 255 // Train RX Server doesn't have a byte
#define SENSOR_COUNT 5			  // Number of Decoders
#define SENSOR_MAX_NUMBER 16	  // Number of Sensors per Decoder

#include "syscall.h"
#include "train_control.h"
#include "util/ring_buffer.h"
#include "util/util.h"

struct sensor_data_publisher_request;

void sensor_read_courier();
void sensor_read_courier_no_cts();
void sensor_query_server();
void print_sensor_prediction_results(int sensor_error, int distance_error);

#endif