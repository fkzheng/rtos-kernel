#include "debug.h"

#define MAX_INT_DIGITS 21

int charsize(const char* ptr) {
	int offset = 0;

	while (*(ptr + offset) != '\0') {
		offset++;
	}

	return offset + 1;
}

static void reverse_string(char* s) {
	// reverses a string of arbitrary length
	int end = 0;
	char temp;
	while (s[end] != '\0') {
		end++;
	};
	for (int i = 0; i < end / 2; ++i) {
		temp = s[i];
		s[i] = s[end - 1 - i];
		s[end - 1 - i] = temp;
	}
}

// reads int n into a string s
void itos(int n, char* s, int want_null_termintor) {
	int i = 0;
	do {
		s[i++] = (n % 10) + '0';
	} while ((n /= 10) > 0);
	char temp = s[i];
	s[i] = '\0';
	reverse_string(s);
	if (!want_null_termintor) {
		s[i] = temp;
	}
}

void prints(char* msg) {
	// int print_tid = WhoIs("UART_TERMINAL_SERVER");
	int print_tid = WhoIs(UART_SERVER_TERM_SEND);

	char replybuf[1];
	struct UART_Server_Request request = create_uart_request(UART_SEND_TERM, msg);
	Send(print_tid, (char*)&request, sizeof(struct UART_Server_Request), replybuf, 1);
}

void kernel_prints(char* msg) {
	uart_puts(0, 0, msg, charsize(msg));
}

void kernel_printi(uint64_t n) {
	char num[MAX_INT_DIGITS];
	for (int i = 0; i < MAX_INT_DIGITS; i++) {
		num[i] = 6 + '0';
	}
	itos(n, num, 1);
	kernel_prints(num);
}

void printi(char* prefix, uint64_t n, char* suffix) {
	// prints a 64 bit integer
	char num[MAX_INT_DIGITS];
	char msg[charsize(prefix) + charsize(suffix) + MAX_INT_DIGITS];
	itos(n, num, 1);

	str_copy(prefix, msg);
	str_cat(msg, num);
	str_cat(msg, suffix);

	prints(msg);
}

void printi_maybe_negative(char* prefix, int n, char* suffix) {
	if (n >= 0) {
		printi(prefix, n, suffix);
	} else {
		char new_prefix[charsize(prefix) + 1];
		str_copy(prefix, new_prefix);
		str_cat(new_prefix, "-");
		printi(new_prefix, n * -1, suffix);
	}
}

void debug_printi_maybe_negative(char* prefix, int i, char* suffix) {
	if (i >= 0) {
		debug_printi(prefix, i, suffix);
	} else {
		char new_prefix[charsize(prefix) + 1];
		str_copy(prefix, new_prefix);
		str_cat(new_prefix, "-");
		debug_printi(new_prefix, i * -1, suffix);
	}
}

void debug_printi(char* prefix, uint64_t n, char* suffix) {
	char save_and_move_cursor[] = "\0337\033[80;1H";
	char restore_cursor[] = "\0338";
	char new_prefix[charsize(prefix) + charsize(save_and_move_cursor)];
	char new_suffix[charsize(suffix) + charsize(restore_cursor)];
	str_copy(save_and_move_cursor, new_prefix);
	str_cat(new_prefix, prefix);
	str_copy(suffix, new_suffix);
	str_cat(new_suffix, restore_cursor);
	printi(new_prefix, n, new_suffix);
}

void debug_prints(char* msg) {
	char save_and_move_cursor[] = "\0337\033[80;1H";
	char restore_cursor[] = "\0338";
	char to_send[charsize(save_and_move_cursor) + charsize(restore_cursor) + charsize(msg)];
	str_copy(save_and_move_cursor, to_send);
	str_cat(to_send, msg);
	str_cat(to_send, restore_cursor);
	prints(to_send);
}

void aFailed(char* file, int line) {
	kernel_prints("ASSERTION FAILED: ");
	kernel_prints(file);
	kernel_prints(":");
	kernel_printi(line);
	kernel_prints("\r\n");

	while (1) {
	}
}