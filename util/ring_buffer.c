#include "ring_buffer.h"

struct Ring_Buffer create_buffer(enum buffer_type type) {
	struct Ring_Buffer buf;
	buf.type = type;
	buf.l = 0;
	buf.r = 0;
	buf.size = 0;
	return buf;
}

int ring_buffer_is_full(struct Ring_Buffer* buf) {
	switch (buf->type) {
	case INT:
		return buf->size == g_max_ibuffer_elements;
	case CHAR:
		return buf->size == g_max_cbuffer_elements;
	default:
		ASSERT(0);
	}

	return -1;
}

int ring_buffer_is_empty(struct Ring_Buffer* buf) {
	return buf->size == 0;
}

uint64_t ring_buffer_pop(struct Ring_Buffer* buf) {
	ASSERT(!ring_buffer_is_empty(buf));
	uint64_t ret;

	switch (buf->type) {
	case INT:
		ret = buf->ibuffer[buf->l];
		buf->l++;
		buf->l %= g_max_ibuffer_elements;
		break;
	case CHAR:
		ret = buf->cbuffer[buf->l];
		buf->l++;
		buf->l %= g_max_cbuffer_elements;
		break;
	default:
		ret = 0;
		ASSERT(0);
		break;
	}

	buf->size--;
	return ret;
}

uint64_t ring_buffer_pop_back(struct Ring_Buffer* buf) {
	ASSERT(!ring_buffer_is_empty(buf));
	uint64_t ret;

	switch (buf->type) {
	case INT:
		ret = buf->ibuffer[buf->r];
		buf->r--;
		break;
	case CHAR:
		ret = buf->cbuffer[buf->r];
		buf->r--;
		break;
	default:
		ret = 0;
		ASSERT(0);
		break;
	}
	if (buf->r < 0) {
		buf->r = g_max_ibuffer_elements - 1;
	}
	buf->size--;
	return ret;
}

void ring_buffer_append(struct Ring_Buffer* buf, uint64_t element) {
	ASSERT(!ring_buffer_is_full(buf));
	switch (buf->type) {
	case INT:
		buf->ibuffer[buf->r] = element;
		buf->r++;
		buf->r %= g_max_ibuffer_elements;
		break;
	case CHAR:
		buf->cbuffer[buf->r] = (char)element;
		buf->r++;
		buf->r %= g_max_cbuffer_elements;
		break;
	default:
		ASSERT(0);
	}

	buf->size++;
}

void ring_buffer_insert_front(struct Ring_Buffer* buf, uint64_t element) {
	ASSERT(!ring_buffer_is_full(buf));
	switch (buf->type) {
	case INT:
		buf->l = (buf->l == 0) ? g_max_ibuffer_elements - 1 : buf->l - 1;
		buf->ibuffer[buf->l] = element;
		break;
	case CHAR:
		buf->l = (buf->l == 0) ? g_max_cbuffer_elements - 1 : buf->l - 1;
		buf->cbuffer[buf->l] = element;
		break;
	default:
		ASSERT(0);
	}

	buf->size++;
}