#ifndef UTIL_H
#define UTIL_H

#include "user/syscall.h"
#include <stddef.h>

#define IDLE_PERIOD_LENGTH 400	   // 400 ms idle periods
#define IDLE_PERIODS_TO_AVERAGE 10 // track idle time over the last 4 seconds

void str_copy(const char* src, char* dest);
int str_equal(const char* str1, const char* str2);
void str_cat(char* src, const char* dest);

void* memset(void* s, int c, size_t n);
void* memcpy(void* restrict dest, const void* restrict src, size_t n);

int is_alphabetical(char c);
int is_number(char c);
int char_to_int(char c);
int parse_possible_three_digit_int(char* start, int* offset);
void get_train_worker_name(int train_num, char* buf);

int min(int a, int b);
int max(int a, int b);

int get_tid_by_name(const char* name);
extern const char UART_SERVER_TRAIN_SEND[];
extern const char UART_SERVER_TRAIN_RECEIVE[];
extern const char UART_SERVER_TERM_SEND[];
extern const char UART_SERVER_TERM_RECEIVE[];

extern const char CLOCK_SERVER[];
extern const char CLOCK_NOTIFIER[];

extern const char TRAIN_ADMIN[];

extern const char TRACK_ADMIN[];
extern const char TRACK_UI_ADMIN[];
#endif