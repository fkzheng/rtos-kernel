#include "util.h"

const char UART_SERVER_TRAIN_SEND[] = "UART1_TX";
const char UART_SERVER_TRAIN_RECEIVE[] = "UART1_RX";
const char UART_SERVER_TERM_SEND[] = "UART0_TX";
const char UART_SERVER_TERM_RECEIVE[] = "UART0_RX";
const char CLOCK_SERVER[] = "CLOCK";
const char CLOCK_NOTIFIER[] = "CLOCK_NOTIF";
const char TRAIN_ADMIN[] = "TRAIN_ADMIN";
const char TRACK_ADMIN[] = "TRACK_ADMIN";
const char TRACK_UI_ADMIN[] = "TRACK_UI_ADMIN";
// define our own memset to avoid SIMD instructions emitted from the compiler
void* memset(void* s, int c, size_t n) {
	for (char* it = (char*)s; n > 0; --n)
		*it++ = c;
	return s;
}

// define our own memcpy to avoid SIMD instructions emitted from the compiler
void* memcpy(void* restrict dest, const void* restrict src, size_t n) {
	char* sit = (char*)src;
	char* cdest = (char*)dest;
	for (size_t i = 0; i < n; ++i)
		*(cdest++) = *(sit++);
	return dest;
}

int min(int a, int b) {
	if (a < b) {
		return a;
	}
	return b;
}

int max(int a, int b) {
	if (a > b) {
		return a;
	}
	return b;
}

void str_copy(const char* src, char* dest) {
	int cur = 0;
	while (src[cur] != '\0') {
		dest[cur] = src[cur];
		cur++;
	}
	dest[cur] = '\0';
}

int str_equal(const char* str1, const char* str2) {
	int p1 = 0;
	int p2 = 0;
	while (str1[p1] != '\0' && str2[p2] != '\0') {
		if (str1[p1] != str2[p2])
			return 0;
		p1++;
		p2++;
	}
	return str1[p1] == '\0' && str2[p2] == '\0';
}

void str_cat(char* src, const char* dest) {
	while (*++src)
		;
	while ((*src++ = *dest++))
		;
}

int is_alphabetical(char c) {
	return (c <= 'z' && c >= 'a') || (c <= 'Z' && c >= 'A');
}

int is_number(char c) {
	return c <= '9' && c >= '0';
}

int char_to_int(char c) {
	return c - '0';
}

int parse_possible_three_digit_int(char* start, int* offset) {
	int res = 0;
	for (int i = 0; i < 3; i++) {
		if (!is_number(start[i])) {
			return res;
		}
		res *= 10;
		res += char_to_int(start[i]);
		*offset += 1;
	}
	return res;
}

void get_train_worker_name(int train_num, char* buf) {
	char name[] = "TRAIN_WORKER_XX";
	name[14] = (train_num % 10) + '0';
	name[13] = (train_num / 10) + '0';
	str_copy(name, buf);
}

int get_tid_by_name(const char* name) {
	int tid = WhoIs(name);
	while (tid == -1) {
		tid = WhoIs(name);
	}
	return tid;
}
