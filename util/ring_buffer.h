#ifndef RING_BUFFER_H
#define RING_BUFFER_H
#include "debug.h" // for asserts
#include <stdint.h>

#define g_max_ibuffer_elements 400
#define g_max_cbuffer_elements 3200

enum buffer_type {
	INT,
	CHAR,
};

struct Ring_Buffer {
	union
	{
		uint64_t ibuffer[g_max_ibuffer_elements];
		char cbuffer[g_max_cbuffer_elements];
	};
	enum buffer_type type;
	int l;
	int r;
	int size;
};

struct Ring_Buffer create_buffer(enum buffer_type type);
int ring_buffer_is_full(struct Ring_Buffer* buf);
int ring_buffer_is_empty(struct Ring_Buffer* buf);
uint64_t ring_buffer_pop(struct Ring_Buffer* buf);
uint64_t ring_buffer_pop_back(struct Ring_Buffer* buf);
void ring_buffer_append(struct Ring_Buffer* buf, uint64_t element);
void ring_buffer_insert_front(struct Ring_Buffer* buf, uint64_t element);

#endif