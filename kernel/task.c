#include "task.h"

struct Task* g_heads[g_total_queues];
struct Task* g_tails[g_total_queues];
uint32_t g_stack_space_for_registers = 0x100;
struct Ring_Buffer g_task_id_buffer;

void init_tasks() {
	// set up ids
	g_task_id_buffer = create_buffer(INT);
	for (int i = 0; i < g_max_task_id; i++) {
		ring_buffer_append(&g_task_id_buffer, i + 1);
	}
	ASSERT(g_task_id_buffer.size == g_max_task_id);

	// set up queues
	for (int i = 0; i < g_total_queues; i++) {
		g_heads[i] = NULL;
		g_tails[i] = NULL;
	}
}

int get_next_task_id() {
	if (ring_buffer_is_empty(&g_task_id_buffer)) {
		// no IDs left
		// assert here b/c we have no error handling :)
		ASSERT(0);
		return -2;
	}

	return ring_buffer_pop(&g_task_id_buffer);
}

void free_task_id(uint64_t id) {
	ring_buffer_append(&g_task_id_buffer, id);
}

void append_task(int priority, struct Task* task) {
	ASSERT(priority >= 0);
	ASSERT(priority < g_total_queues);
	// adds task to back of the queue
	if (g_heads[priority] == NULL && g_tails[priority] == NULL) {
		// this is the first task of this priority, replace head as well
		g_heads[priority] = task;
	} else {
		g_tails[priority]->next_task = task;
	}
	g_tails[priority] = task;
}

struct Task* pop_task_by_arg(int priority, uint64_t arg, enum which_pop wp) {
	ASSERT(priority == g_blocked_receive || priority == g_blocked_send || priority == g_blocked_reply
		   || priority == g_blocked_event);
	struct Task* cur = g_heads[priority];
	struct Task* prev = NULL;

	while (cur != NULL) {
		if ((wp == TASK_ID && cur->task_id == arg)
			|| (wp == SEND_ID && cur->request != NULL && cur->request->arg1 == arg)
			|| (wp == EVENT_ID && cur->request != NULL && cur->request->arg1 == arg)) {
			// we make the implicit assumption that the last element in the
			// queue always has next_task = NULL
			if (prev != NULL) {
				prev->next_task = cur->next_task;
			}
			if (cur == g_heads[priority]) {
				g_heads[priority] = cur->next_task;
			}
			if (cur == g_tails[priority]) {
				g_tails[priority] = prev;
			}

			cur->next_task = NULL;
			return cur;
		}
		prev = cur;
		cur = cur->next_task;
	}
	return NULL; // did not find
}

struct Task* pop_task(int priority) {
	// priority must have at least one task
	ASSERT(g_heads[priority] != NULL);

	struct Task* ret = g_heads[priority];
	g_heads[priority] = g_heads[priority]->next_task;

	if (g_heads[priority] == NULL) {
		// only task left in this priority, delete tail as well
		g_tails[priority] = NULL;
	}

	ret->next_task = NULL;

	return ret;
}

int has_tasks() {
	// if theres only blocked tasks, we're done
	for (int i = 0; i < g_max_task_priority; i++) {
		if (g_heads[i] != NULL) {
			return 1;
		}
	}
	return 0;
}

struct Task* top_task() {
	// priority must have at least one task
	for (int i = 0; i < g_max_task_priority; i++) {
		if (g_heads[i] != NULL) {
			return pop_task(i);
		}
	}
	prints("FAILED TO GET A TASK");
	ASSERT(0);
	return NULL; // suppress warning
}

int create_and_add_task(int priority, void (*function)(), uint64_t parent_tid) {
	struct Task* task = (struct Task*)get_next_free_section();

	// set up task fields
	task->task_id = get_next_task_id();
	task->parent_task_id = parent_tid;
	task->return_value = 0;
	task->next_task = NULL;
	task->request = NULL;
	task->entry_point = function;
	task->stack_bottom = bottom_of_section(task) - g_stack_space_for_registers;
	task->spsr = 0;
	task->priority = priority;

	// add to task queue
	append_task(priority, task);
	ASSERT(g_heads[priority] != NULL);

	// print_task(task);
	return task->task_id;
}
