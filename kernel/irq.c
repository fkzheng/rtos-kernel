#include "irq.h"

// Interrupts
#define UART_INTERRUPT 145
#define TIMER_INTERRUPT 97
#define GIC_SPURIOUS_ACK 1023

// UART IIR Values

struct Syscall_Request* handle_interrupt() {
	uint32_t interrupt_number = get_interrupt_number();
	struct Task* handler_task;
	char irq0, irq1;

	switch (interrupt_number) {
	case UART_INTERRUPT:
		irq0 = read_uart_irq(0);
		irq1 = read_uart_irq(1);

		if ((irq0 & (1)) && (irq1 & (1))) {
			ack_uart_interrupt();
			return NULL;
		}

		// TODO: this is garbage, merge both channels IIR into one value and send to notifier
		if (irq1 == 194) { // THR interrupt
			kernel_disable_tx_interrupt(1);

			handler_task = pop_task_by_arg(g_blocked_event, interrupt_number, EVENT_ID);
			ASSERT(handler_task != NULL);
			handler_task->return_value = irq1 | (1 << 8);
			append_task(handler_task->priority, handler_task);
		} else if (irq1 == 204 || irq1 == 196) { // RX interrupts
			kernel_disable_rx_interrupt(1);
			handler_task = pop_task_by_arg(g_blocked_event, interrupt_number, EVENT_ID);
			ASSERT(handler_task != NULL);
			handler_task->return_value = irq1 | (1 << 8);
			append_task(handler_task->priority, handler_task);
		} else if (irq1 == 192 || irq1 == 224) { // modem interrupt
			if (irq1 == 192 && is_cts_delta()) {
				kernel_disable_modem_interrupt(1);
				handler_task = pop_task_by_arg(g_blocked_event, interrupt_number, EVENT_ID);
				ASSERT(handler_task != NULL);
				handler_task->return_value = irq1 | (1 << 8);
				append_task(handler_task->priority, handler_task);
			} else {
				ack_uart_interrupt();
			}
		} else {
			if (irq1 != 193)
				kernel_printi(irq1);
		}

		if (irq0 == 194) {
			kernel_disable_tx_interrupt(0);
			handler_task = pop_task_by_arg(g_blocked_event, interrupt_number, EVENT_ID);
			ASSERT(handler_task != NULL);
			handler_task->return_value = irq0;
			append_task(handler_task->priority, handler_task);
		} else if (irq0 == 204 || irq0 == 196) { // RX interrupts
			kernel_disable_rx_interrupt(0);
			handler_task = pop_task_by_arg(g_blocked_event, interrupt_number, EVENT_ID);
			ASSERT(handler_task != NULL);
			handler_task->return_value = irq0;
			append_task(handler_task->priority, handler_task);
		} else {
			if (irq0 != 193)
				kernel_printi(irq0);
		}
		break;
	case TIMER_INTERRUPT:
		handler_task = pop_task_by_arg(g_blocked_event, interrupt_number, EVENT_ID);
		ASSERT(handler_task != NULL);
		append_task(handler_task->priority, handler_task);
		break;
	case GIC_SPURIOUS_ACK:
		break;
	default:
		ASSERT(0);
		break;
	}
	return NULL;
}
