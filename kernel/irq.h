#ifndef IRQ_H
#define IRQ_H

#include "task.h"
#include "user/request.h"
#include "util/debug.h"
#include "util/rpi.h"
#include <stdint.h>

struct Syscall_Request* handle_interrupt();

#endif