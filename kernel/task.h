#ifndef TASK_H
#define TASK_H
#include "mem_allocator.h"
#include "user/request.h"
#include "util/debug.h"
#include "util/ring_buffer.h"
#include <stddef.h>
#include <stdint.h>

#define g_max_task_priority 5

// The task has executed Receive(), and is waiting for a task to sent to it.
#define g_blocked_receive g_max_task_priority
// The task has executed Send(), and is waiting for the message to be received.
#define g_blocked_send g_max_task_priority + 1
// The task has executed Send() and its message has been received, but it has not received a reply.
#define g_blocked_reply g_max_task_priority + 2
// The task has executed AwaitEvent()
#define g_blocked_event g_max_task_priority + 3

#define g_max_task_id 200 // largest possible ID for a task
#define g_total_queues 9

struct Task {
	// stuff we read in asm
	uint64_t return_value;
	char* stack_bottom;
	uint64_t spsr;
	void (*entry_point)();

	uint64_t task_id;
	struct Task* next_task;
	uint64_t parent_task_id;
	uint64_t priority;
	struct Syscall_Request* request;
};

enum which_pop {
	TASK_ID,  // pop task with task_id == arg
	SEND_ID,  // pop task with request->arg1 == arg
	EVENT_ID, // pop task with request->arg1 == arg
};

void init_tasks();
int create_and_add_task(int priority, void (*function)(), uint64_t parent_tid);
void append_task(int priority, struct Task* task);
int has_tasks();
struct Task* top_task();
struct Task* pop_task_by_arg(int priority, uint64_t arg, enum which_pop wp);
void free_task_id(uint64_t id);

#endif