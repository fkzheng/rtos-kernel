#include "kernel.h"

uint64_t g_idle_time;
uint64_t g_idle_start_time;
uint32_t g_idle_flag;
uint32_t g_idle_count;
uint32_t g_idle_period_start;
struct Ring_Buffer g_idle_time_buf;
struct Ring_Buffer g_idle_time_starts_buf;

struct Syscall_Request* activate(struct Task* currtask) {
	struct Syscall_Request* request = enter_user_mode(currtask);
	// exception vector table should jump here

	return request;
}

void send_message(struct Task* send_task, struct Task* receive_task) {
	int send_len = min(receive_task->request->arg3, send_task->request->arg3);
	memcpy((char*)receive_task->request->arg2, (char*)send_task->request->arg2, send_len);
	*((int*)receive_task->request->arg1) = send_task->task_id;
	receive_task->return_value = send_len;
}

void reply_message(struct Task* send_task, struct Task* receive_task) {
	int send_len = min(receive_task->request->arg5, send_task->request->arg3);
	memcpy((char*)receive_task->request->arg4, (char*)send_task->request->arg2, send_len);
	receive_task->return_value = send_len;
	send_task->return_value = send_len;
}

int handle(struct Syscall_Request* request, struct Task* currtask) {
	if (request == NULL) {
		append_task(currtask->priority, currtask);
		return 0;
	}
	currtask->request = request;
	if (request->request_id == EXIT) {
		free_task_id(currtask->task_id);
		free_section(currtask);
		return 0;
	} else if (request->request_id == QUIT) {
		return 1;
	} else if (request->request_id == YIELD) {
		// nop, just reschedule
		append_task(currtask->priority, currtask);
	} else if (request->request_id == CREATE) {
		int new_task_id = create_and_add_task((int)request->arg1, (void (*)())request->arg2, currtask->task_id);
		currtask->return_value = new_task_id;
		append_task(currtask->priority, currtask);
	} else if (request->request_id == MY_TID) {
		currtask->return_value = currtask->task_id;
		append_task(currtask->priority, currtask);
	} else if (request->request_id == PARENT_TID) {
		currtask->return_value = currtask->parent_task_id;
		append_task(currtask->priority, currtask);
	} else if (request->request_id == SEND) {
		int receive_id = (int)request->arg1;
		struct Task* receive_task = pop_task_by_arg(g_blocked_receive, receive_id, TASK_ID);
		if (receive_task == NULL) {
			// task has not received yet
			append_task(g_blocked_send, currtask);
			return 0;
		}
		// copy message to receiver and append both
		send_message(currtask, receive_task);
		append_task(receive_task->priority, receive_task);
		append_task(g_blocked_reply, currtask);
	} else if (request->request_id == RECEIVE) {
		struct Task* send_task = pop_task_by_arg(g_blocked_send, currtask->task_id, SEND_ID);
		if (send_task == NULL) {
			append_task(g_blocked_receive, currtask);
			return 0;
		}

		send_message(send_task, currtask);
		append_task(currtask->priority, currtask);
		append_task(g_blocked_reply, send_task);
	} else if (request->request_id == REPLY) {
		struct Task* receive_task = pop_task_by_arg(g_blocked_reply, (int)currtask->request->arg1, TASK_ID);
		ASSERT(receive_task != NULL);
		// reply
		reply_message(currtask, receive_task);
		append_task(receive_task->priority, receive_task);
		append_task(currtask->priority, currtask);
	} else if (request->request_id == AWAIT_EVENT) {
		append_task(g_blocked_event, currtask);
	} else if (request->request_id == IDLE) {
		append_task(currtask->priority, currtask);
	} else if (request->request_id == GET_IDLE_TIME) {
		uint64_t total_idle = 0;
		for (int i = 0; i < g_idle_time_buf.size; i++) {
			total_idle += g_idle_time_buf.ibuffer[(g_idle_time_buf.l + i) % g_max_ibuffer_elements];
		}
		uint64_t start_time = g_idle_time_starts_buf.ibuffer[g_idle_time_starts_buf.l] >> 32;
		int ind = g_idle_time_starts_buf.r - 1;
		if (ind < 0) {
			ind = g_max_ibuffer_elements - 1;
		}
		uint64_t end_time = g_idle_time_starts_buf.ibuffer[ind] & 0xFFFFFFFF;
		total_idle = (total_idle * 10000) / (end_time - start_time);
		currtask->return_value = total_idle;
		append_task(currtask->priority, currtask);
	} else if (request->request_id == WRITE_REGISTER) {
		currtask->return_value = 0;
		uart_write_register(0, request->arg1, request->arg2, request->arg3);
		append_task(currtask->priority, currtask);
	} else if (request->request_id == READ_REGISTER) {
		currtask->return_value = uart_read_register(0, request->arg1, request->arg2);
		append_task(currtask->priority, currtask);
	} else {
		kernel_prints("GOT REQUEST: ");
		kernel_printi(request->request_id);
		kernel_prints("\r\n");
		ASSERT(request->request_id == 0);
		ASSERT(request->request_id == CREATE);
		ASSERT(0);
	}
	return 0;
}

void init_idle_time_tracking() {
	g_idle_time = 0;
	g_idle_start_time = 0;
	g_idle_flag = 0;
	g_idle_count = 0;
	g_idle_period_start = 0;
	g_idle_time_buf = create_buffer(INT);
	g_idle_time_starts_buf = create_buffer(INT);
}

void setup_kernel_tasks() {
	kernel_prints("Bootstrap init\r\n");
	create_and_add_task(3, &bootstrap_tc1, 0);
}

struct Task* schedule() {
	struct Task* scheduled_task = top_task();
	uint32_t curtime = get_true_timestamp();

	if (scheduled_task->task_id == g_idle_server_tid) {
		if (g_idle_flag == 0) {
			g_idle_flag = 1;
		} else {
			g_idle_time += (curtime - g_idle_start_time);
		}
		g_idle_start_time = curtime;
	} else if (g_idle_flag == 1) {
		g_idle_flag = 0;
		g_idle_time += (curtime - g_idle_start_time);
		g_idle_start_time = curtime;
	}

	if (curtime >= g_idle_period_start + (IDLE_PERIOD_LENGTH * 1000)) {
		ring_buffer_append(&g_idle_time_buf, g_idle_time);
		uint64_t res = g_idle_period_start;
		res = res << 32;
		res |= curtime;
		ring_buffer_append(&g_idle_time_starts_buf, res);
		g_idle_time = 0;
		g_idle_period_start = curtime;
		if (g_idle_time_buf.size > IDLE_PERIODS_TO_AVERAGE) {
			if (ring_buffer_is_empty(&g_idle_time_buf)) {
				ASSERT(0);
			}
			ring_buffer_pop(&g_idle_time_buf);
			if (ring_buffer_is_empty(&g_idle_time_starts_buf)) {
				ASSERT(0);
			}
			ring_buffer_pop(&g_idle_time_starts_buf);
		}
	}
	return scheduled_task;
}

void kmain() {
	struct Task* currtask;
	struct Syscall_Request* request;
	int res;
	while (has_tasks()) {
		currtask = schedule();
		request = activate(currtask);
		res = handle(request, currtask);
		if (res == 1) {
			break;
		}
	}
}