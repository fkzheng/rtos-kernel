#ifndef MEM_ALLOCATOR_H
#define MEM_ALLOCATOR_H

void* get_next_free_section();
void free_section(void* section);
char* bottom_of_section(void* section);
void init_memory();

#endif