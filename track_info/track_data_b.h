#ifndef TRACK_DATA_B_H
#define TRACK_DATA_B_H
/* THIS FILE IS GENERATED CODE -- DO NOT EDIT */

#include "stdint.h"
#include "track_node.h"

// The track initialization functions expect an array of this size.
#define TRACK_MAX_B 140

void init_trackb(track_node* track);

extern const uint8_t g_branch_info_b[22][2];

#endif