#ifndef TRACK_H
#define TRACK_H

#include "track_data_a.h"
#include "track_data_b.h"
#include "track_node.h"
#include "util/debug.h"

extern track_node track_nodes[];
extern char which_track;
extern uint8_t TRACK_NODE_COUNT;

void init_track(char track);

#endif