#ifndef TRACK_DATA_A_H
#define TRACK_DATA_A_H
/* THIS FILE IS GENERATED CODE -- DO NOT EDIT */

#include "stdint.h"
#include "track_node.h"

// The track initialization functions expect an array of this size.
#define TRACK_MAX_A 144

void init_tracka(track_node* track);

extern const uint8_t g_branch_info_a[22][2];
#endif