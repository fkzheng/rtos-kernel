#include "track.h"

track_node track_nodes[144]; // 13824 bytes == 13kB

char which_track = 'a';
uint8_t TRACK_NODE_COUNT = TRACK_MAX_A;

void init_track(char track) {
	if (track == 'a') {
		init_tracka(track_nodes);
	} else if (track == 'b') {
		which_track = 'b';
		TRACK_NODE_COUNT = TRACK_MAX_B;
		init_trackb(track_nodes);
	} else {
		prints("invalid track id\r\n");
	}
}