#include "kernel/kernel.h"
#include "kernel/mem_allocator.h"
#include "kernel/task.h"
#include "tests/test.h"
#include "user/bootstrap.h"
#include "user/request.h"
#include "util/debug.h"
#include "util/rpi.h"
#include "util/util.h"

// exit the kernel to run user task, then return on synchronous exception (interrupt later)

void init() {
	init_memory();
	init_tasks();
	init_gic();
	init_timer();
	init_idle_time_tracking();
	// init_util();
}

int main() {
	init_gpio();
	init_spi(0);
	init_uart(0);
	init();
	kernel_prints("\033[2J");

#ifdef RUN_TESTS
	run_all_tests();
#else
	setup_kernel_tasks();
	kmain();

	kernel_prints("\r\nExiting Main\r\n");
#endif

	return 0;
}
