# CS452 Kernel
Group members: Harrison Chen (hh6chen) and Franklin Zheng (fkzheng)

# How to access and make
1. clone the repository and checkout the commit hash
2. Build the `.img` file with `make clean all` in the same directory as this README. 
3. Upload the image to one of the RPis with `./upload <pi>` where `<pi>` is one of the IDs of the RPis, 0,1,2, or 3.
4. Restart the RPi and then restart the marklin box by holding the stop/go button for 3 seconds.
