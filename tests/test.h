#ifndef TEST_H
#define TEST_H

// #define RUN_TESTS // comment this line to get rid of compilation for testing
#ifdef RUN_TESTS

#include "kernel/kernel.h"
#include "kernel/mem_allocator.h"
#include "kernel/task.h"
#include "user/bootstrap.h"
#include "user/syscall.h"
#include "util/debug.h"
#include "util/ring_buffer.h"

void run_all_tests();
#endif
#endif
