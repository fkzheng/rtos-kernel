#include "test.h"
#ifdef RUN_TESTS

// syscall tests
void _user_syscall_exit() {
	Exit();
}

void test_exit() {
	struct Syscall_Request _exit_req = create_request(4, 0, 0, 0, 0, 0);
	uint64_t task = create_and_add_task(0, &_user_syscall_exit, 0);

	struct Task* td = top_task();
	ASSERT(task == td->task_id);
	struct Syscall_Request* req = activate(td);
	ASSERT(_exit_req.request_id == req->request_id);
	ASSERT(_exit_req.arg1 == req->arg1);
	ASSERT(_exit_req.arg2 == req->arg2);
	ASSERT(_exit_req.arg3 == req->arg3);
	ASSERT(_exit_req.arg4 == req->arg4);
	ASSERT(_exit_req.arg5 == req->arg5);
}

void test_syscalls() {
	test_exit();

	// struct Syscall_Request expected_exit_req = create_request(4, 0, 0, 0, 0, 0);
	// struct Syscall_Request expected_exit_req = create_request(4, 0, 0, 0, 0, 0);
	// struct Syscall_Request expected_exit_req = create_request(4, 0, 0, 0, 0, 0);
	// struct Syscall_Request expected_exit_req = create_request(4, 0, 0, 0, 0, 0);

	// int task1 = create_and_add_task(int priority, void (*function)(), uint64_t parent_tid);
	// int task1 = create_and_add_task(int priority, void (*function)(), uint64_t parent_tid);
	// int task1 = create_and_add_task(int priority, void (*function)(), uint64_t parent_tid);
	// int task1 = create_and_add_task(int priority, void (*function)(), uint64_t parent_tid);

	kernel_prints("Passed Test syscalls\r\n");
}

// mem_allocator tests
void test_mem_allocator() {
	char* first_mem = get_next_free_section();
	char* second_mem = get_next_free_section();

	ASSERT(second_mem - first_mem == 128000);
	free_section(first_mem);
	char* third_mem = get_next_free_section();
	char* fourth_mem = get_next_free_section();
	ASSERT(first_mem == third_mem);
	ASSERT(fourth_mem - second_mem == 128000);

	kernel_prints("Passed Test mem_allocator\r\n");
}

void _dummy_fn() { }

// task creation tests
void test_task_creation() {

	create_and_add_task(0, _dummy_fn, 0);
	create_and_add_task(0, _dummy_fn, 0);
	create_and_add_task(0, _dummy_fn, 0);
	create_and_add_task(0, _dummy_fn, 0);

	struct Task* task1 = top_task();
	struct Task* task2 = top_task();
	struct Task* task3 = top_task();
	struct Task* task4 = top_task();

	append_task(0, task1);
	append_task(0, task2);
	append_task(0, task3);
	append_task(0, task4);

	struct Task* task5 = top_task();
	struct Task* task6 = top_task();
	struct Task* task7 = top_task();
	struct Task* task8 = top_task();

	ASSERT(task5 == task1);
	ASSERT(task6 == task2);
	ASSERT(task7 == task3);
	ASSERT(task8 == task4);

	kernel_prints("Passed Test: test_task_creation\r\n");
}

// queue_test for task_id popping
void test_queue_popping() {
	create_and_add_task(0, _dummy_fn, 0);
	create_and_add_task(0, _dummy_fn, 0);
	create_and_add_task(0, _dummy_fn, 0);
	create_and_add_task(0, _dummy_fn, 0);
	struct Task* task = top_task();
	append_task(g_blocked_send, task);
	ASSERT(task->task_id >= 1);
	ASSERT(pop_task_by_arg(g_blocked_send, task->task_id, TASK_ID) == task);
	ASSERT(pop_task_by_arg(g_blocked_send, task->task_id, TASK_ID) == NULL);
	ASSERT(pop_task_by_arg(g_blocked_receive, task->task_id, TASK_ID) == NULL);
	ASSERT(pop_task_by_arg(g_blocked_reply, task->task_id, TASK_ID) == NULL);
	ASSERT(pop_task_by_arg(g_blocked_send, task->task_id, SEND_ID) == NULL);
	ASSERT(pop_task_by_arg(g_blocked_receive, task->task_id, SEND_ID) == NULL);
	ASSERT(pop_task_by_arg(g_blocked_reply, task->task_id, SEND_ID) == NULL);

	struct Task* task1 = top_task();
	struct Task* task2 = top_task();
	struct Task* task3 = top_task();
	append_task(g_blocked_receive, task1);
	append_task(g_blocked_receive, task2);
	append_task(g_blocked_receive, task3);

	struct Task* task4 = pop_task_by_arg(g_blocked_receive, task2->task_id, TASK_ID);
	ASSERT(task4 == task2);
	struct Syscall_Request r = create_request(SEND, 12, 0, 0, 0, 0);
	task3->request = &r;
	ASSERT(pop_task_by_arg(g_blocked_receive, 12, SEND_ID) == task3);
	ASSERT(pop_task_by_arg(g_blocked_receive, task1->task_id, TASK_ID) == task1);
	ASSERT(pop_task_by_arg(g_blocked_receive, task1->task_id, TASK_ID) == NULL);

	kernel_prints("Passed Test: test_queue_popping\r\n");
}

// message passing
void message_receiving_task() {
	char expected_s[] = "o goh meng nigel\r\n";
	char buf[20];
	int sender_tid;
	int res = Receive(&sender_tid, buf, 20);
	ASSERT(res == 19);
	for (int i = 0; i < res; i++) {
		ASSERT(buf[i] == expected_s[i]);
	}
	char reply[] = "my name nigel\r\n";
	res = Reply(sender_tid, reply, charsize(reply));
	ASSERT(res == 16);
	Exit();
}

void message_passing_task() {
	int id = create_and_add_task(0, message_receiving_task, 5);
	char msg[] = "o goh meng nigel\r\n";
	char replybuf[20];
	int res = Send(id, msg, charsize(msg), replybuf, 20);
	ASSERT(res == 16);
	Exit();
}

void test_message_passing() {
	init_tasks();
	create_and_add_task(0, message_passing_task, 0);
	kmain();
	kernel_prints("Passed Test: test_message_passing\r\n");
}

void message_passing_task2() {
	char msg[] = "send";
	char replybuf[20];
	// for (int i = 0; i < 5; i++) {
	int res = Send(1, msg, 5, replybuf, 20);
	ASSERT(res == 8);
	//}
	Exit();
}

void message_receiving_task2() {
	int child_id = create_and_add_task(2, message_passing_task2, 1);
	char buf[20];
	int sender_tid;
	// for (int i = 0; i < 5; i++) {
	int res = Receive(&sender_tid, buf, 20);
	ASSERT(res == 5);
	ASSERT(sender_tid == child_id);
	char reply[] = "reply\r\n";
	res = Reply(sender_tid, reply, charsize(reply));
	ASSERT(res == charsize(reply));
	Exit();
}

void test_message_passing2() {
	init_tasks();
	create_and_add_task(0, message_receiving_task2, 0);
	kmain();
	kernel_prints("Passed Test: test_message_passing2\r\n");
}

void child_task() {
	int tid = MyTid();
	int sender_tid;
	char buf[1];
	Receive(&sender_tid, buf, 1);
	ASSERT(buf[0] == tid);
	int parent_tid = MyParentTid();
	Reply(parent_tid, &parent_tid, 1);
	Exit();
}

void parent_task() {
	int my_tid = MyTid();
	int child1 = create_and_add_task(0, child_task, my_tid);
	int child2 = create_and_add_task(0, child_task, my_tid);
	int child3 = create_and_add_task(0, child_task, my_tid);

	char msg = child1;
	char reply[1];
	Send(child1, &msg, 1, reply, 1);
	ASSERT(reply[0] == MyTid());

	msg = child2;
	Send(child2, &msg, 1, reply, 1);
	ASSERT(reply[0] == MyTid());

	msg = child3;
	Send(child3, &msg, 1, reply, 1);
	ASSERT(reply[0] == MyTid());
	Exit();
}

void test_message_passing3() {
	init_tasks();
	create_and_add_task(0, parent_task, 0);
	kmain();
	kernel_prints("Passed Test: test_message_passing3\r\n");
}

// uint64_t ring buffer tests
void test_ring_buffer() {
	uint64_t num = 123;
	struct Ring_Buffer buf = create_buffer(INT);
	ASSERT(ring_buffer_is_empty(&buf));

	ring_buffer_append(&buf, num);
	num = ring_buffer_pop(&buf);

	ASSERT(ring_buffer_is_empty(&buf));
	ASSERT(num == 123);

	int i;
	for (i = 0; i < g_max_ibuffer_elements; i++) {
		ring_buffer_append(&buf, num);
	}
	ASSERT(ring_buffer_is_full(&buf));
	kernel_prints("Passed Test: test_ring_buffer\r\n");
}

// test for nameserver
void test_name_server_task() {
	int my_tid = MyTid();
	RegisterAs("Ligma");
	int n = WhoIs("Ligma2");
	ASSERT(n == 3);
	n = WhoIs("unknown");
	ASSERT(n == -1);
	n = WhoIs("Ligma");
	ASSERT(n == my_tid);
	Unregister();
	ASSERT(WhoIs("Ligma") == -1);

	Exit();
}

void test_name_server_task2() {
	int my_tid = MyTid();
	RegisterAs("Ligma2");
	int n = WhoIs("Ligma");
	ASSERT(n == 2);
	n = WhoIs("Ligm");
	ASSERT(n == -1);
	n = WhoIs("Ligma2");
	ASSERT(n == my_tid);
	Exit();
}

void test_name_server() {
	init_tasks();
	create_and_add_task(0, name_server, 0);
	create_and_add_task(1, test_name_server_task, 0);
	create_and_add_task(1, test_name_server_task2, 0);
	kmain();
	kernel_prints("Passed Test: test_name_server\r\n");
}

void test_clock_server_client() {
	prints("checkpoint 1\r\n");
	int clock_server_tid = WhoIs(CLOCK_SERVER);
	prints("checkpoint 2\r\n");
	ASSERT(clock_server_tid != -1);
	uint32_t curtime = Time(clock_server_tid);
	prints("checkpoint 3\r\n");
	printi("ticks: ", get_timestamp() - curtime, "\r\n");
	ASSERT(get_timestamp() - curtime <= 2);
	prints("checkpoint 4\r\n");

	Exit();
}

void test_clock_server() {
	init_tasks();
	create_and_add_task(0, name_server, 0);
	create_and_add_task(0, uart_term_server_w, 0);
	create_and_add_task(1, clock_server, 0);
	create_and_add_task(1, clock_notifier, 0);
	create_and_add_task(2, test_clock_server_client, 0);
	init_timer();
	kmain();
	kernel_prints("Passed Test: test_clock_server\r\n");
}

// run test suite from main
void run_all_tests() {
	kernel_prints("Runnning Tests:\r\n");
	test_mem_allocator();
	test_task_creation();
	test_syscalls();
	test_ring_buffer();
	test_queue_popping();
	test_message_passing();
	test_message_passing2();
	test_message_passing3();
	test_name_server();
	test_clock_server();
}

#endif
